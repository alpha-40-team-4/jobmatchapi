# JobMatchAPI


## Description

JobMatchAPI is a RESTFul API that provides the functionality for a web-based job board. 
Companies and professionals can register and post job listings on the platform. 
Those listings can be filtered and searched based on a number of search criteria:
- location,
- salary range,
- skills & requirements, etc.

Companies and professionals can send match requests to job listings and choose with whom 
to match.

There is also an admin role with access to admin endpoints that can delete users, ads,
skills/requirements, etc; block/unblock users; approve new registrations and suggested
new skills/requirements.


## Technologies

* Python
* FastAPI Web Framework
* MariaDB & SQL
* Pydantic library for data validation
* JWC for token encryption
* OAuth2 for authentication
* Pytest

* You can find detailed library in the 'requirements.txt' file

## Setup

1. Clone the repo - `https://gitlab.com/alpha-40-team-4/jobmatchapi.git`
2. Install MariaDB - `https://www.mariadbtutorial.com/getting-started/install-mariadb/`
3. Install the requirements - `pip install -r requirements.txt`
4. Run MariaDB. Connect locally.
5. Dump the `sql_job_match_script.sql` schema into your local database
6. Fill in data with a dump from `job_match_data_script.sql`
7. Access the interactive documentation through `http://127.0.0.1:8000/docs#/` with uvicorn server 
   running locally

## Functionality Overview

### Users
* Professionals registration and login
* Companies registration and login
* Admin login

* Each user can view their respective profile info:
    - for professionals - user info, number of own active company ads and received match
  requests from companies (if marked visible)
    - for companies - user info, number of active job ads and number of matches
    - for admin - basic user info only
* Companies and professionals can edit their respective profile info
* Companies and professionals can view own sent match requests
* Each user can search for professionals and companies
* Admin can view detailed profile info of a specific user

### Company ads
* Professionals can create company ads (with status - active, private, hidden)
* Professionals can edit their company ads
* Users can view active/private company ad by id (with its match requests to the author or admin)
* Professionals can view own company ads
* Admin can view all company ads of a professional
* Companies and admin can search for active company ads by:
    - min-max salary range (+ threshold)
    - location
    - remote
    - skills and their respective levels (+ threshold)


### Job ads
* Companies can create job ads
* Companies can edit their own job ads
* Users can view specific job ad (with its match requests to the author or admin)
* Companies can view their own job ads
* Admin can view all job ads of a company
* Professionals and admin can search job ads by:
    - min-max salary range (+ threshold)
    - location
    - remote
    - requirements and their respective levels (+ threshold)


### Match requests
* Companies can send match requests to an active/private professionals company ad
* Professionals can send match requests to an active companies job ad
* Companies can accept match requests from a professional
* Professionals can accept match requests from a company

### Admins
* View all users with pending registrations
* Approve pending registrations
* View blocked users
* Block/Unblock users
* Delete user by id
* Delete company or job ad by id

### System Functionality
* Users can view available locations
* Users can add locations as pending or active status if admin
* Admin can delete locations
* Admin can approve pending locations
* Admin can view pending locations
* Users can view active skills/requirements
* Users can add skills/requirements as pending or active if admin
* Admin can delete skills/requirements
* Admin can approve pending skills/requirements
* Admin can view all pending skills/requirements
* Users can view skills/requirements levels

### Additional Functionality
* The system sends automatic email to a recipient when they have a match
* The system publishes an automatic tweet when a job ad is posted by a company


## Authors
* Mariya Kamenarova
* Plamen Gunchev
* Nora Andonova