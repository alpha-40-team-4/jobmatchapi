# pylint: disable=missing-module-docstring
# pylint: disable=import-error

from fastapi import HTTPException, status
from src.data import database
from src.data.app_models import JobAdMatchRequest, CompanyAdMatchRequest
from src.data.user_models import ProfessionalRegData, CompanyRegData, UserData
from src.data.info_models import CompanyInfo, ProfInfo
from src.data.company_ad_models import CompAdsStatusAll
from src.data.job_ad_models import JobAdStatusAll
from src.common import security


def register_company(data: CompanyRegData):
    """
    Registers new user and company info in DB or raises HTTPException
    if email is already registered.\n
    :param data: company registration model
    :return: company info model
    """
    with database.get_connection() as conn:
        try:
            hashed_password = security.encode_password(data.password)
            company_id = database.process_query_transactional(
                'INSERT INTO users (username,password,user_types_id) VALUES(?,?,?)',
                conn,
                (data.username, hashed_password, 2))
            company_info_id = database.process_query_transactional(
                'INSERT INTO companies (users_id, company_name, email) VALUES(?,?,?)',
                conn,
                (company_id, data.company_name, data.email))
            conn.commit()
            return CompanyInfo(id=company_id, name=data.company_name, email=data.email)

        except Exception as e:
            conn.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=e.args[0])


def register_professional(data: ProfessionalRegData):
    """
    Registers new user and professional info in DB or raises HTTPException
    if email is already registered.\n
    :param data: professional registration model
    :return: professional info model
    """
    with database.get_connection() as conn:
        try:
            hashed_password = security.encode_password(data.password)
            professional_id = database.process_query_transactional(
                'INSERT INTO users (username,password,user_types_id) VALUES(?,?,?)',
                conn,
                (data.username, hashed_password, 1))
            professional_info_id = database.process_query_transactional(
                '''INSERT INTO professionals (users_id, first_name, last_name, email)
                 VALUES(?,?,?,?)''',
                conn,
                (professional_id, data.first_name, data.last_name, data.email))
            conn.commit()
            return ProfInfo(id=professional_id, first_name=data.first_name,
                            last_name=data.last_name, email=data.email)

        except Exception as e:
            conn.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=e.args[0])


def register_professional_wrapper(data: ProfessionalRegData):
    """
    Returns registered professional info data or raises HTTPException if
    username is already registered.\n
    :param data: professional registration model
    :return: professional info
    """
    name_exists_check = get_user_by_username(data.username)
    if name_exists_check is not None:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                            detail=f'Username {data.username} already exists')
    result = register_professional(data)

    return result


def register_company_wrapper(data: CompanyRegData):
    """
    Returns registered company info data or raises HTTPException if
    username is already registered.\n
    :param data: company registration model
    :return: company info
    """
    name_exists_check = get_user_by_username(data.username)
    if name_exists_check is not None:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                            detail=f'Username {data.username} already exists')
    result = register_company(data)

    return result


def get_user_by_username(username: str) -> UserData | None:
    """
    Returns user data by given username from DB. Returns None if there is
    no such user.\n
    :param username: str
    :return: UserData or None
    """
    data = database.read_query('SELECT * FROM users WHERE username = ?', (username,))
    return next((UserData.from_query_result(*row) for row in data), None)


def get_user_by_id(user_id: int) -> UserData | None:
    """
    Returns user data by given ID from DB. Returns None if there is
    no such user.\n
    :param user_id: int
    :return: UserData or None
    """
    data = database.read_query('SELECT * FROM users WHERE id = ?', (user_id,))
    return next((UserData.from_query_result(*row) for row in data), None)


def get_company_by_id(company_id: int):
    """
    Returns company info by given ID from DB. Returns None if there is
    no such company.\n
    :param company_id: int
    :return: company info or None
    """
    data = database.read_query(
        '''SELECT users_id, company_name, description, email, contacts,picture_logo, location_id
        FROM companies WHERE users_id = ?''',
        (company_id,)
    )

    return next((CompanyInfo.from_query_result(*row) for row in data), None)


def get_professional_by_id(professional_id: int) -> ProfInfo | None:
    """
    Returns professional info by given ID from DB. Returns None if there is
    no such professional.\n
    :param professional_id: int
    :return: professional info or None
    """
    data = database.read_query(
        '''SELECT users_id, summary, active, photo, location_id, email, first_name, last_name,
        main_company_ad_id, visible_matches
        FROM professionals WHERE users_id = ?''',
        (professional_id,)
    )

    return next((ProfInfo.from_query_result(*row) for row in data), None)


def get_pending_registrations():
    """
    Returns list of users with pending status in DB.\n
    :return: list[user data]
    """
    data = database.read_query(
        'SELECT * FROM users WHERE approved = ? and deleted = ?', (False, False))

    result = [UserData.from_query_result(*row) for row in data]

    return [user.copy(exclude={'hashed_password', 'deleted'}) for user in result]


def confirm_user_registration(user_ids: list[int]):
    """
    Updates users in DB setting approved status to True for provided user IDs.
    Raises HTTPException if users are already approved.\n
    :param user_ids: list of users IDs
    :return: str message
    """
    result = database.update_query(
        f"""UPDATE users SET approved = ?
        WHERE id IN ({','.join('?' * len(user_ids))}) AND deleted = ?""",
        (True, *user_ids, False))

    if result == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Registrations already confirmed, users do not exist or were deleted')

    return 'Registrations approved'


def get_blocked_users() -> list[UserData]:
    """
    Returns list of blocked users from DB.\n
    :return: list[user data]
    """
    data = database.read_query(
        'SELECT * FROM users WHERE blocked = ? and deleted = ?', (True, False))

    result = [UserData.from_query_result(*row) for row in data]

    return [user.copy(exclude={'hashed_password', 'deleted'}) for user in result]


def update_blocked_unblocked(blocked: bool, user_ids: list[int]):
    """
    Updates blocked status of users in DB or raises HTTPException if
    no change is applied.\n
    :param blocked: True/False for block/unblock
    :param user_ids: list of users IDs
    :return: str message
    """
    result = database.update_query(
        f"""UPDATE users SET blocked = ?
        WHERE id IN ({','.join('?' * len(user_ids))}) AND deleted = ?""",
        (blocked, *user_ids, False))

    if result == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Users are already blocked/unblocked, do not exist or were deleted')

    return f'Blocked status set successfully to {blocked} for users {user_ids}'


def delete_users(user_ids: list[int]):
    """
    Set deleted to True in DB for the given users IDs or raises HTTPException
    if users are already flagged deleted.\n
    :param user_ids: list of users IDs
    :return: str message
    """
    result = database.update_query(
        f"UPDATE users SET deleted = ? WHERE id IN ({','.join('?' * len(user_ids))})",
        (True, *user_ids))

    if result == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Users are already deleted or do not exist')

    return f'Users {user_ids} deleted'


def delete_comp_ad(comp_ad_id: int):
    """
    Set deleted to True in DB for the given company ad ID or raises HTTPException
    if ad is matched, already flagged deleted or does not exist.\n
    :param comp_ad_id: ad ID
    :return: str message
    """
    result = database.update_query(
        'UPDATE company_ads SET status = ? WHERE id = ? AND status != ?',
        (CompAdsStatusAll.DELETED.value, comp_ad_id, CompAdsStatusAll.MATCHED.value))

    if result == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Can not delete ad. It is matched, deleted or does not exist')

    return f'Ad with id: {comp_ad_id} deleted'


def del_job_ad(job_ad_id: int):
    """
    Set deleted to True in DB for the given job ad ID or raises HTTPException
    if ad is matched, already flagged deleted or does not exist.\n
    :param job_ad_id: ad ID
    :return: str message
    """
    result = database.update_query(
        'UPDATE job_ads SET status = ? WHERE id = ? AND status != ?',
        (JobAdStatusAll.DELETED.value, job_ad_id, JobAdStatusAll.MATCHED.value))

    if result == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Can not delete ad. It is matched, deleted or does not exist')

    return f'Ad with id: {job_ad_id} deleted'


def get_sent_requests_by_prof_id(user_id: int):
    """
    Returns job ad match requests sent from the professional with the given ID.\n
    :param user_id: user ID
    :return: list[match requests]
    """
    data = database.read_query(
        '''SELECT id, matched, users_id, job_ad_id FROM match_requests WHERE users_id = ?''',
        (user_id,))
    return [JobAdMatchRequest.from_query(*row) for row in data]


def get_sent_requests_by_company_id(user_id: int):
    """
    Returns company ad match requests sent from the company with the given ID.\n
    :param user_id: user ID
    :return: list[match requests]
    """
    data = database.read_query(
        '''SELECT id, matched, users_id, company_ad_id FROM match_requests WHERE users_id = ?''',
        (user_id,))
    return [CompanyAdMatchRequest.from_query(*row) for row in data]
