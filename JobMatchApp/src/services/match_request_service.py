# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=import-error

from mariadb import Error
from fastapi import HTTPException, status
from src.data import database
from src.data.app_models import CompanyAdMatchRequest, JobAdMatchRequest
from src.data.company_ad_models import CompAdsStatusAll
from src.common.exceptions import ForbiddenException
from src.services import mail_integration_service, company_ad_service, job_ad_service


def create_company_ad_request_wrapper(company_id: int, company_ad_id: int):
    """
    Checks if: \n
    - The same company_ad_request already exists
    - Company_ad is eligible to receive a request
    Initiates company_ad_request creation. Returns result to router.
    :param company_id:
    :param company_ad_id:
    :return:
    """
    check_exists = check_if_company_ad_request_already_exists(company_id, company_ad_id)
    check_eligibility = company_ad_service.check_company_ad_eligibility(company_ad_id)

    if check_exists is None and check_eligibility is None:
        company_ad_request = create_company_ad_request(company_id, company_ad_id)

        return company_ad_request


def create_job_ad_request_wrapper(professional_id: int, job_ad_id: int) -> HTTPException | \
                                                                           JobAdMatchRequest:
    """
    Checks if: \n
    - The same job_ad_request already exists
    - Job_ad is eligible to receive a request
    Initiates job_ad_request creation. Returns result to router.
    :param professional_id:
    :param job_ad_id:
    :return: HTTPException | JobAdMatchRequest
    """
    check_exists = check_if_job_ad_request_already_exists(professional_id, job_ad_id)
    check_eligibility = job_ad_service.check_job_ad_eligibility(job_ad_id)

    if check_exists is None and check_eligibility is None:
        job_ad_request = create_job_ad_request(professional_id, job_ad_id)

        return job_ad_request


def match_with_professional_wrapper(match_request_id: int, company_id: int) -> \
        HTTPException | ForbiddenException | str:
    """
    Checks if: \n
    - Logged-in user is the job ad author
    - Match_request is not yet matched
    Initiates a match with professional. Returns result to router.
    :param company_id:
    :param match_request_id:
    :return: HTTPException | ForbiddenException | str
    """
    job_match_request_repr = get_job_match_request_by_id(match_request_id)
    company_is_author_check = check_if_company_is_job_ad_author(job_match_request_repr, company_id)
    match_request_status_check = check_if_match_request_status_is_false(match_request_id)

    if company_is_author_check is None and match_request_status_check is None:
        prof_match = match_with_professional(job_match_request_repr)

        return prof_match


def match_with_company_wrapper(match_request_id: int, professional_id: int) -> \
        HTTPException | ForbiddenException | str:
    """
    Checks if: \n
    - Logged-in user is the company ad author
    - Match_request is not yet matched
    Initiates a match with a company. Returns result to router.
    :param match_request_id:
    :param professional_id:
    :return: HTTPException | ForbiddenException | str
    """
    company_match_request_repr = get_company_match_request_by_id(match_request_id)
    professional_is_author_check = check_if_professional_is_company_ad_author(
        company_match_request_repr, professional_id
    )
    match_request_status_check = check_if_match_request_status_is_false(match_request_id)

    if professional_is_author_check is None and match_request_status_check is None:
        company_match = match_with_company(company_match_request_repr, professional_id)

        return company_match


def create_company_ad_request(company_id: int, company_ad_id: int) -> CompanyAdMatchRequest:
    """
    A company sends match_request to a company_ad. Sends email notification
    to the company_ad author (professional). \n
    :param company_id:
    :param company_ad_id:
    :return: CompanyAdMatchRequest
    """
    generated_match_request_id = database.insert_query(
        '''INSERT INTO match_requests(matched, users_id, company_ad_id) VALUES(?, ?, ?)''',
        (False, company_id, company_ad_id)
    )
    if generated_match_request_id:
        mail_integration_service.send_match_email_to_professional(company_ad_id)

    return CompanyAdMatchRequest.from_query(id=generated_match_request_id,
                                            matched=False,
                                            user_id=company_id,
                                            company_ad_id=company_ad_id)


def create_job_ad_request(professional_id: int, job_ad_id: int):
    """
    A professional sends match_request to a job_ad. Sends email notification
    to the job_ad author (company). \n
    :param professional_id:
    :param job_ad_id:
    :return: JobMatchRequest
    """
    generated_match_request_id = database.insert_query(
        '''INSERT INTO match_requests(matched, users_id, job_ad_id) VALUES(?, ?, ?)''',
        (False, professional_id, job_ad_id)
    )
    if generated_match_request_id:
        mail_integration_service.send_match_email_to_company(job_ad_id)

    return JobAdMatchRequest.from_query(id=generated_match_request_id,
                                        matched=False,
                                        user_id=professional_id,
                                        job_ad_id=job_ad_id)


def check_if_company_ad_request_already_exists(company_id: int,
                                               company_ad_id: int) -> HTTPException | None:
    """
    Checks if the same company_ad_match_request already exists. \n
    :param company_id:
    :param company_ad_id:
    :return: HTTPException | None
    """
    data = database.read_query(
        '''SELECT 1 FROM match_requests WHERE users_id = ? AND company_ad_id = ?''',
        (company_id, company_ad_id)
    )
    if data:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'You have already sent a request to company ad with id {company_ad_id}'
        )

    return None


def check_if_job_ad_request_already_exists(professional_id: int,
                                           job_ad_id: int) -> HTTPException | None:
    """
    Checks if the same job_ad_match_request already exists. \n
    :param professional_id:
    :param job_ad_id:
    :return: HTTPException | None
    """
    data = database.read_query(
        '''SELECT 1 FROM match_requests WHERE users_id = ? AND job_ad_id = ?''',
        (professional_id, job_ad_id)
    )
    if data:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'You have already sent a request to job ad with id {job_ad_id}'
        )

    return None


def get_match_requests(comp_ads_ids: list[int]) -> list[CompanyAdMatchRequest]:
    """
    Receives a list of company ids. Returns a list of all
    company_ad_match_requests sent by those companies. \n
    :param comp_ads_ids:
    :return: list[CompanyAdMatchRequest]
    """
    data = database.read_query(
        '''SELECT id, matched, users_id, company_ad_id FROM match_requests
        WHERE company_ad_id in (?)''',
        (*comp_ads_ids,))

    return [CompanyAdMatchRequest.from_query(*row) for row in data]


#   ----------------------
#   MATCH A REQUEST CHECKS
#   ----------------------


def check_if_match_request_status_is_false(match_request_id: int) -> None | HTTPException:
    """
    Checks if match_request status is available for matching. \n
    :param match_request_id:
    :return: None | HTTPException
    """
    data = database.read_query('''SELECT 1 FROM match_requests WHERE id = ? AND matched = false''',
                               (match_request_id,))
    if not data:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'Match request with id {match_request_id} is already matched'
        )

    return None


#   ---------------------------------
#   MATCH A PROFESSIONAL AS A COMPANY
#   ---------------------------------


def get_job_match_request_by_id(match_request_id: int) -> JobAdMatchRequest | HTTPException:
    """
    Returns an existing job_ad_match_request sent by a professional. \n
    :param match_request_id:
    :return: JobAdMatchRequest | HTTPException
    """
    data = database.read_query(
        '''SELECT id, matched, users_id, job_ad_id FROM match_requests WHERE id = ?''',
        (match_request_id,)
    )
    job_match_request_repr = next((JobAdMatchRequest.from_query(*row) for row in data), None)

    if job_match_request_repr is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Request with id {match_request_id} not found'
        )

    return job_match_request_repr


def check_if_company_is_job_ad_author(job_match_request_repr: JobAdMatchRequest,
                                      user_id: int) -> None | ForbiddenException:
    """
    Checks if the logged-in company is the intended recipient of a job_ad_match_request. \n
    :param job_match_request_repr:
    :param user_id:
    :return: None | ForbiddenException
    """
    data = database.read_query('''SELECT 1 FROM job_ads WHERE id = ? AND companies_users_id = ?''',
                               (job_match_request_repr.job_ad_id, user_id))
    if not data:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'''Your company is not the recipient of a match request
                    with id {job_match_request_repr.id}'''
        )

    return None


def match_with_professional(job_match_request_repr: JobAdMatchRequest) -> str | HTTPException:
    """
    Logged-in company approves a job_ad_match_request under one of its respective job_ads. \n
    Upon successful match:
     - Job_ad status is changed to 'matched'
     - Professional (request author) status 'active' is set to 'false'
     - Job_ad_match_request status 'matched' is set to 'true'
     - All Professional's (request author) active company_ads are set to 'hidden'
    :param job_match_request_repr:
    :return: str | HTTPException
    """
    with database.get_connection() as conn:
        result = []
        try:
            job_ad_status_update = database.update_query_transactional(
                '''UPDATE job_ads SET status = "matched" WHERE id = ?''',
                conn, (job_match_request_repr.job_ad_id,)
            )
            result.append(job_ad_status_update)

            professional_status_update = database.update_query_transactional(
                '''UPDATE professionals SET active = false WHERE users_id = ?''',
                conn, (job_match_request_repr.user_id,))
            result.append(professional_status_update)

            match_request_status_update = database.update_query_transactional(
                '''UPDATE match_requests SET matched = true WHERE id = ?''',
                conn, (job_match_request_repr.id,))
            result.append(match_request_status_update)

            if all(result):
                database.update_query_transactional(
                    '''UPDATE company_ads SET status = "hidden" WHERE professionals_users_id = ?
                        AND status = "active"''',
                    conn, (job_match_request_repr.user_id,))
                conn.commit()
                return 'You have a match!'

            raise Error('Failed to match')

        except Error as ex:
            conn.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=ex)


#   ------------------------------------
#   MATCH A COMPANY AD AS A PROFESSIONAL
#   ------------------------------------


def get_company_match_request_by_id(match_request_id: int) -> CompanyAdMatchRequest | HTTPException:
    """
    Returns an existing company_ad_match_request sent by a company. \n
    :param match_request_id:
    :return: CompanyAdMatchRequest | HTTPException
    """
    data = database.read_query('''SELECT m.id, m.matched, m.users_id, m.company_ad_id
                                  FROM match_requests m
                                  JOIN company_ads c ON c.id = m.company_ad_id WHERE m.id = ?
                                  AND c.status IN (?,?)''',
                               (match_request_id, CompAdsStatusAll.ACTIVE.value,
                                CompAdsStatusAll.PRIVATE.value))
    company_match_request_repr = next((CompanyAdMatchRequest.from_query(*row) for row in data),
                                      None)

    if company_match_request_repr is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Request with id {match_request_id} not found'
        )

    return company_match_request_repr


def check_if_professional_is_company_ad_author(company_match_request_repr: CompanyAdMatchRequest,
                                               user_id: int) -> None | ForbiddenException:
    """
    Checks if the logged-in professional is the intended recipient of a company_ad_match_request. \n
    :param company_match_request_repr:
    :param user_id:
    :return: None | ForbiddenException
    """
    data = database.read_query('''SELECT 1 FROM company_ads WHERE id = ?
                                  AND professionals_users_id = ?''',
                               (company_match_request_repr.company_ad_id, user_id))
    if not data:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'''Your are not the recipient of a match request
                    with id {company_match_request_repr.id}'''
        )

    return None


def match_with_company(company_match_request_repr: CompanyAdMatchRequest, professional_id: int) ->\
        str | HTTPException:
    """
    Logged-in professional approves a company_ad_match_request under one of its
    respective company_ads. \n
    Upon successful match:
     - Company_ad status is changed to 'matched'
     - Professional status 'active' is set to 'false'
     - Company_ad_match_request status 'matched' is set to 'true'
     - All Professional's active company_ads are set to 'hidden'
    :param company_match_request_repr:
    :param professional_id:
    :return: str | HTTPException
    """
    with database.get_connection() as conn:
        result = []
        try:
            company_ad_status_update = database.update_query_transactional(
                '''UPDATE company_ads SET status = ? WHERE id = ?''', conn,
                (CompAdsStatusAll.MATCHED.value, company_match_request_repr.company_ad_id)
            )
            result.append(company_ad_status_update)

            professional_status_update = database.update_query_transactional(
                '''UPDATE professionals SET active = false WHERE users_id = ?''',
                conn, (professional_id,)
            )
            result.append(professional_status_update)

            match_request_status_update = database.update_query_transactional(
                '''UPDATE match_requests SET matched = true WHERE id = ?''',
                conn, (company_match_request_repr.id,)
            )
            result.append(match_request_status_update)

            if all(result):
                hide_all_professional_company_ads = database.update_query_transactional(
                    '''UPDATE company_ads SET status = ?
                    WHERE professionals_users_id = ? and status = ?''',
                    conn,
                    (CompAdsStatusAll.HIDDEN.value, professional_id, CompAdsStatusAll.ACTIVE.value)
                )
                conn.commit()
                return 'You have a match!'
            
            raise Error('Failed to match')

        except Error as ex:
            conn.rollback()
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=ex)
