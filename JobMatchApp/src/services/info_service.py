# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=invalid-name

from fastapi import HTTPException
from starlette import status

from src.services import job_ad_service
from src.services import users_service
from src.services import company_ad_service
from src.services import match_request_service
from src.data import database
from src.data.info_models import CompanyInfo, ProfInfo, CompanyInfoUpdate, ProfInfoUpdate, \
    CompanyInfoFull, ProfInfoFull
from src.data.user_models import UserData
from src.data.company_ad_models import CompAdsStatus, CompAdsStatusAll
from src.data.app_models import CompanyAdMatchRequest
from src.data.job_ad_models import JobAdStatus, JobAdStatusAll


def update_company(company_info: CompanyInfo, company_info_update: CompanyInfoUpdate):
    """
    Updates company info in DB.\n
    :param company_info: existing company info
    :param company_info_update: incoming updates
    :return: updated company info model or a message if no actual update
    """
    if company_info_update.logo:
        company_info.logo = database.upload_image(company_info_update.logo)
        company_info_update.logo = None

    updated_company = company_info.copy(update=company_info_update.dict(exclude_none=True))

    result = database.update_query(
        '''UPDATE companies SET company_name = ?, description = ?, contacts = ?,
        picture_logo = ?, location_id = ? WHERE users_id = ?''',
        (updated_company.name, updated_company.description, updated_company.contacts,
         updated_company.logo, updated_company.location_id, company_info.id))
    if result == 0:
        return 'No changes made to company info'
    return updated_company


def update_prof(prof_info: ProfInfo, prof_info_update: ProfInfoUpdate):
    """
    Updates professional info in DB or raises HTTPException if main_company_ad
    is updated with an ad from different user or ad status is not 'active'.\n
    :param prof_info: existing professional info
    :param prof_info_update: incoming updates
    :return: updated professional info model or a message if no actual update
    """
    if prof_info_update.main_company_ad_id:
        ad = company_ad_service.return_ad_by_id(prof_info_update.main_company_ad_id)
        if ad.user_id != prof_info.id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f'User does not have an ad with id: {prof_info_update.main_company_ad_id}'
            )
        if ad.status != CompAdsStatus.ACTIVE:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Only active company ads could be set to 'main'"
            )

    if prof_info_update.photo:
        prof_info.photo = database.upload_image(prof_info_update.photo)
        prof_info_update.photo = None

    updated_prof = prof_info.copy(update=prof_info_update.dict(exclude_none=True))

    result = database.update_query(
        '''UPDATE professionals SET summary = ?, active = ?, photo = ?, location_id = ?,
        first_name = ?, last_name = ?, main_company_ad_id = ?, visible_matches = ?
        WHERE users_id = ?''',
        (updated_prof.summary, updated_prof.active, updated_prof.photo,
         updated_prof.location_id, updated_prof.first_name, updated_prof.last_name,
         updated_prof.main_company_ad_id, updated_prof.visible_matches, prof_info.id))

    if result == 0:
        return 'No changes made to professional info'

    return updated_prof


def get_user_info_by_id(user_id: int):
    """
    Returns detailed info of user profile in DB or raises HTTPException
    if user is deleted or does not exist. \n
    :param user_id: user ID
    :return: dict with user info
    """
    user_data = users_service.get_user_by_id(user_id)

    if user_data is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'User with id: {user_id} does not exist')

    if user_data.deleted is True:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'User with id: {user_id} is deleted')

    user = get_user_info_by_userdata(user_data)

    return {'username': user_data.username,
            'approved': user_data.approved,
            'blocked': user_data.blocked,
            'user info': user}


def get_user_info_by_userdata(user: UserData):
    """
    Returns detailed info of user profile in DB depending on user type
    or raises HTTPException if user info does not exist. \n
    :param user: user's user data
    :return: dict with user info, active ads count and matched ads if user
        is professional and matches are visible, dict with user info, active ads
        count and matches count if user is company or dict with username and username
        if user is admin
    """
    if user.user_type_id == 1:
        user_profile = get_professional_info(user.id)
    elif user.user_type_id == 2:
        user_profile = get_company_info(user.id)
    elif user.user_type_id == 3:
        user_profile = user.copy(include={'username', 'user_type_id'})
    else:
        user_profile = None

    if not user_profile:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='User not found')

    return user_profile


def get_professional_info(user_id: int):
    """
    Returns detailed professional info with active ads count and matched ads
    if matches are visible or None if user does not exist. \n
    :param user_id: user ID
    :return: dict
    """
    user_info = users_service.get_professional_by_id(user_id)
    if user_info is None:
        return None

    ads = company_ad_service.get_ads_by_user_id(user_id)

    if user_info.visible_matches is True:
        matched_ids = [ad.id for ad in ads if ad.status == CompAdsStatusAll.MATCHED]
        return {'user info': user_info,
                'number of active ads': len(
                    [ad for ad in ads if ad.status == CompAdsStatusAll.ACTIVE]
                ),
                'matched ads': match_request_service.get_match_requests(matched_ids)}

    return {'user info': user_info,
            'number of active ads': len(
                [ad for ad in ads if ad.status == CompAdsStatusAll.ACTIVE]
            )}

def get_company_info(user_id: int):
    """
    Returns detailed company info with active ads count and matched ads
    count. \n
    :param user_id: user ID
    :return: dict
    """
    user_info = users_service.get_company_by_id(user_id)
    if user_info is None:
        return None
    ads = job_ad_service.get_ads_by_user_id(user_id)

    return {'user info': user_info,
            'number of active ads': len(
                [ad for ad in ads if ad.status == JobAdStatusAll.ACTIVE.value]
            ),
            'number of matched ads': len(
                [ad for ad in ads if ad.status == JobAdStatusAll.MATCHED.value]
            )}


def search_companies(name: str | None, description: str | None, location_id: int | None):
    """
    Filter company profiles by name, description and/or location ID. \n
    :param name: name of the company (or part of name)
    :param description: description (or part of description)
    :param location_id: location ID
    :return: list of company profiles
    """
    sql = '''SELECT c.users_id, c.company_name, c.description,
          c.email, c.contacts, c.picture_logo, c.location_id
          FROM companies c JOIN users u ON u.id = c.users_id
          WHERE u.approved=? AND u.blocked=? AND u.deleted=?'''
    sql_params = [True, False, False]

    if name:
        sql += ' AND c.company_name LIKE ?'
        sql_params += [f'%{name}%']
    if description:
        sql += ' AND c.description LIKE ?'
        sql_params += [f'%{description}%']
    if location_id:
        sql += ' AND c.location_id=?'
        sql_params += [location_id]

    data = database.read_query(sql, sql_params)
    active_ads_data = database.read_query(
        '''SELECT companies_users_id, count(id) FROM job_ads
        WHERE status = ? GROUP BY companies_users_id''',
        (JobAdStatus.ACTIVE.value,))
    matched_data = database.read_query(
        '''SELECT companies_users_id, count(id) FROM job_ads
        WHERE status = ? GROUP BY companies_users_id''',
        (JobAdStatus.MATCHED.value,))

    result = []
    for row in data:
        comp_id = row[0]
        num_active_ads = next((item[1] for item in active_ads_data if item[0] == comp_id), None)
        num_matches = next((item[1] for item in matched_data if item[0] == comp_id), None)
        result.append(CompanyInfoFull.from_query_result(*row, num_active_ads, num_matches))

    return result


def search_professionals(first_name: str | None, last_name: str | None, summary: str | None,
                         location_id: int | None):
    """
    Filter professionals profiles by first name, last name, summary and/or location ID. \n
    :param first_name: first name of professional (or part of name)
    :param last_name: last name of professional (or part of name)
    :param summary: summary (or part of summary)
    :param location_id: location ID
    :return: list of professionals profiles
    """
    sql = '''SELECT p.users_id, p.summary, p.active, p.photo, p.location_id, p.email,
          p.first_name, p.last_name, p.main_company_ad_id, p.visible_matches
          FROM professionals p JOIN users u ON u.id = p.users_id
          WHERE u.approved=? AND u.blocked=? AND u.deleted=?'''
    sql_params = [True, False, False]

    if first_name:
        sql += ' AND p.first_name LIKE ?'
        sql_params += [f'%{first_name}%']
    if last_name:
        sql += ' AND p.first_name LIKE ?'
        sql_params += [f'%{last_name}%']
    if summary:
        sql += ' AND p.summary LIKE ?'
        sql_params += [f'%{summary}%']
    if location_id:
        sql += ' AND p.location_id=?'
        sql_params += [location_id]

    data = database.read_query(sql, sql_params)
    active_ads_data = database.read_query(
        '''SELECT professionals_users_id, count(id) FROM company_ads
        WHERE status=? GROUP BY professionals_users_id''',
        (CompAdsStatus.ACTIVE.value,))
    matched_data = database.read_query(
        '''SELECT c.professionals_users_id, m.id, m.matched, m.users_id,
        m.company_ad_id from match_requests m
        JOIN company_ads c ON c.id=m.company_ad_id WHERE matched=?''',
        (True,))

    matched_ads = {}
    for row in matched_data:
        user_id = row[0]
        if user_id in matched_ads:
            matched_ads[user_id].append(CompanyAdMatchRequest.from_query(*row[1:]))
        else:
            matched_ads[user_id] = [CompanyAdMatchRequest.from_query(*row[1:])]

    result = []
    for row in data:
        user_id = row[0]
        num_active_ads = next((item[1] for item in active_ads_data if item[0] == user_id), None)
        matches = matched_ads.get(user_id)
        result.append(ProfInfoFull.from_query_result(*row, num_active_ads, matches))
    return result
