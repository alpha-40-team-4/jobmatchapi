# pylint: disable=missing-module-docstring
# pylint: disable=import-error

import os
import tweepy
from dotenv import load_dotenv
from src.data import database

load_dotenv()

consumer_key = os.getenv('CONSUMER_KEY')
consumer_secret = os.getenv('CONSUMER_SECRET')

my_access_token = os.getenv('ACCESS_TOKEN')
my_access_token_secret = os.getenv('ACCESS_TOKEN_SECRET')


def create_tweet(company_name: str, job_ad_id: int) -> dict:
    """
    Posts an announcement tweet from the system's Twitter account upon job_ad creation.
    :param company_name:
    :param job_ad_id:
    :return: dict | Response
    """
    client = tweepy.Client(consumer_key=consumer_key, consumer_secret=consumer_secret,
                           access_token=my_access_token, access_token_secret=my_access_token_secret)
    response = client.create_tweet(text=f'''Our partner company {company_name} just posted
                                         a new opportunity! Check it out here:
                                         https://jobpostapi.com/{job_ad_id}''')
    return response


def get_job_ad_creator(user_id: int) -> str:
    """
    Fetches the username of the job_ad author (company)
    :param user_id:
    :return: str
    """
    data = database.read_query('SELECT username FROM users WHERE id = ?', (user_id,))
    return data[0][0]
