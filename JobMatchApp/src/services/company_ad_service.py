# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=unused-variable
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
# pylint: disable=too-many-arguments

from collections import Counter
from fastapi import HTTPException, status
from mariadb import Error
from src.data import database
from src.data.company_ad_models import CompanyAdCreate, CompanyAd, CompanyAdUpdate, \
    CompanyAdWithRequests, CompAdsStatusAll
from src.data.app_models import Skill, MatchRequest, SkillStatus
from src.data.user_models import UserData
from src.services import users_service


def create(c_ad: CompanyAdCreate, user_id: int):
    """
    Creates new company ad in DB. \n
    :param c_ad: new company ad
    :param user_id: user ID of the author
    :return: company ad
    """
    with database.get_connection() as conn:
        try:
            ad_id = database.process_query_transactional(
                '''INSERT INTO company_ads(salary_min, salary_max, description, location_id,
                remote, status, professionals_users_id) VALUES(?,?,?,?,?,?,?)''', conn,
                (c_ad.salary_min, c_ad.salary_max, c_ad.description, c_ad.location_id, c_ad.remote,
                 c_ad.status, user_id))
            skills = [database.process_query_transactional(
                '''INSERT INTO company_ads_has_skills(company_ads_id, skills_id, levels_id) VALUES
                (?,(SELECT s.id FROM skills s WHERE s.id=? AND s.status=?),?)''', conn,
                (ad_id, skill.skill_id, SkillStatus.ACTIVE.value, skill.level_id))
                for skill in c_ad.skills_levels]
            conn.commit()
        except Error as ex:
            conn.rollback()
            raise ex

    return CompanyAd.from_query(ad_id, c_ad.salary_min, c_ad.salary_max, c_ad.description,
                                c_ad.remote, c_ad.status, c_ad.location_id, user_id,
                                c_ad.skills_levels)


def update(ad_id: int, ad_update: CompanyAdUpdate, user_id: int):
    """
    Updates company ad in DB (skills can be added or updated on level) or
    raises HTTPException if user is not author of the ad or ad is already matched. \n
    :param ad_id: ID of the ad to be updated
    :param ad_update: updated fields of the ad
    :param user_id: user ID
    :return: company ad
    """
    c_ad = return_ad_by_id(ad_id)
    if c_ad.user_id != user_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'User with id: {user_id} is not the author of the ad and can not modify it')
    if c_ad.status == CompAdsStatusAll.MATCHED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'Ad with id: {ad_id} is already matched and could not be modified')
    current_skills = c_ad.skills_levels
    skills_update = ad_update.skills_levels
    updated_ad = c_ad.copy(update=ad_update.dict(exclude_none=True))

    if c_ad.status == CompAdsStatusAll.ACTIVE and c_ad.status != updated_ad.status:
        with database.get_connection() as conn:
            try:
                ad_result = database.update_query_transactional(
                    '''UPDATE company_ads SET salary_min=?, salary_max=?, description=?, remote=?,
                    status=?, location_id=? WHERE id=?''', conn,
                    (updated_ad.salary_min, updated_ad.salary_max, updated_ad.description,
                     updated_ad.remote, updated_ad.status, updated_ad.location_id, ad_id))
                info_result = database.update_query_transactional(
                    'UPDATE professionals SET main_company_ad_id = ? WHERE users_id = ?',
                    conn, (None, user_id))
                if ad_result or info_result:
                    conn.commit()
            except Error as ex:
                conn.rollback()
                raise ex
    else:
        ad_result = database.update_query(
            '''UPDATE company_ads SET salary_min=?, salary_max=?, description=?, remote=?,
            status=?, location_id=? WHERE id=?''',
            (updated_ad.salary_min, updated_ad.salary_max, updated_ad.description,
             updated_ad.remote, updated_ad.status, updated_ad.location_id, ad_id))

    if skills_update:
        updates = []
        new = []
        for skill in skills_update:
            if skill.skill_id in [s.skill_id for s in current_skills]:
                updates.append(skill)
            else:
                new.append(skill)
        with database.get_connection() as conn:
            try:
                update_result = [database.update_query_transactional(
                    '''UPDATE company_ads_has_skills SET levels_id=?
                    WHERE company_ads_id=? AND skills_id=?''',
                    conn, (skill.level_id, ad_id, skill.skill_id)) for skill in updates]
                skills_result = [database.process_query_transactional(
                    '''INSERT INTO company_ads_has_skills(company_ads_id, skills_id, levels_id)
                    VALUES
                    (?,(SELECT s.id FROM skills s WHERE s.id=? AND s.status=?),?)''', conn,
                    (ad_id, skill.skill_id, SkillStatus.ACTIVE.value, skill.level_id))
                    for skill in new]
                if update_result or skills_result:
                    conn.commit()

                    skills_ids = set(
                        [s.skill_id for s in current_skills]
                        + [s.skill_id for s in skills_update]
                    )
                    skills_levels = []
                    for skill_id in skills_ids:
                        current = next(
                            (s for s in current_skills if s.skill_id == skill_id), None
                        )
                        updated = next(
                            (s for s in skills_update if s.skill_id == skill_id), None
                        )
                        skills_levels.append(updated if updated else current)

                    updated_ad.skills_levels = skills_levels
            except Error as ex:
                conn.rollback()
                raise ex

    return updated_ad


def get_ad_by_id(ad_id: int, user: UserData) -> CompanyAdWithRequests | CompanyAd:
    """
    Returns company ad with match requests to it if user is author or admin,
    company ad without match requests to other users if ad status is 'active'
    or 'private' or raises HTTPException if ad status is 'hidden' or 'deleted'.\n
    :param ad_id: company ad ID
    :param user: user - requester to get the ad
    :return: company ad
    """
    c_ad = return_ad_by_id(ad_id)

    if c_ad.user_id == user.id or user.user_type_id == 3:
        requests_data = database.read_query(
            'SELECT id, matched, users_id FROM match_requests WHERE company_ad_id = ?', (ad_id,))
        matches = [MatchRequest.from_query(*row) for row in requests_data]
        res = CompanyAdWithRequests.from_ad(c_ad, match_requests=matches)
        return res

    if c_ad.status in [CompAdsStatusAll.HIDDEN, CompAdsStatusAll.MATCHED]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'Company Ad with id: {ad_id} is visible only to its author')
    return c_ad


def get_ads_by_user_id(user_id: int) -> list[CompanyAd]:
    """
    Returns list of company ads with author user_id. \n
    :param user_id: user ID
    :return: list of company ads
    """
    data = database.read_query(
        '''SELECT c.*, s.skills_id, s.levels_id FROM company_ads c
        JOIN company_ads_has_skills s ON c.id = s.company_ads_id
        JOIN skills ON skills.id = s.skills_id
        WHERE c.professionals_users_id = ? AND c.status != ? AND skills.status = ?
        ORDER BY CASE c.status WHEN 'active' THEN 1 WHEN 'private' THEN 2 WHEN 'hidden'
        THEN 3 WHEN 'matched' THEN 4 END, id''',
        (user_id, CompAdsStatusAll.DELETED.value, SkillStatus.ACTIVE.value))

    if data is None:
        return []

    return _return_ads_from_data(data)


def get_ads_by_user_id_admin(user_id: int) -> list[CompanyAd]:
    """
    Returns list of company ads with author user_id or raises HTTPException if
    user does not exist, is deleted or is not registered as professional. \n
    :param user_id: user_id: user ID
    :return: list of company ads
    """
    user = users_service.get_user_by_id(user_id)
    if user is None or user.deleted is True:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with id: {user_id} does not exist')
    if user.user_type_id != 1:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'User with id: {user_id} is not registered as professional')

    return get_ads_by_user_id(user_id)


def search_comp_ads(min_salary: int | None, max_salary: int | None, salary_threshold: float,
                    location_id: int | None, remote: bool | None,
                    skills: list[str], skills_threshold: int):
    """
    Returns list of filtered company ads by given params or raises
    HTTPException if max salary is lower than min salary.\n
    :param min_salary: minimal search salary
    :param max_salary: maximal search salary
    :param salary_threshold: [0, 0.99] applied on salary search params
    :param location_id: location ID
    :param remote: remote option
    :param skills: ['1,1'] list of skill-level ids
    :param skills_threshold: skills threshold
    :return: list of company ads
    """
    if min_salary and max_salary and max_salary < min_salary:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Max salary should be greater than min salary')
    if skills_threshold >= len(skills):
        skills = []
        skills_threshold = 0

    return get_ads(min_salary, max_salary, salary_threshold, location_id, remote,
                   skills, skills_threshold)


def get_ads(min_salary: int | None, max_salary: int | None, salary_threshold: float,
            location_id: int | None, remote: bool | None, skills: list[str], skills_threshold: int):
    """
        Filter company ads from DB by min salary, max salary, salary threshold,
        location ID, remote status, skills and/or skills threshold.
        Salary threshold widens search interval for salary with the given percentage,
        Skills threshold (t) filters all ads with (n-t) skills, where n is the number
        of skills list.\n
        :param min_salary: minimal search salary
        :param max_salary: maximal search salary
        :param salary_threshold: [0, 0.99] applied on salary search params
        :param location_id: location ID
        :param remote: remote option
        :param skills: ['1,1'] list of skill-level ids
        :param skills_threshold: skills threshold
        :return: list of company ads
        """
    sql = '''SELECT c.id FROM company_ads c
            JOIN company_ads_has_skills s ON c.id = s.company_ads_id
            JOIN skills ON skills.id = s.skills_id
            WHERE c.status = ? AND skills.status = ?'''
    sql_params = [CompAdsStatusAll.ACTIVE.value, SkillStatus.ACTIVE.value]

    if min_salary:
        sql += ' AND c.salary_max >= ?'
        sql_params += [min_salary * (1 - salary_threshold)]
    if max_salary:
        sql += ' AND c.salary_min <= ?'
        sql_params += [max_salary * (1 + salary_threshold)]

    if location_id:
        if remote is None:
            sql += ' AND (c.location_id = ? OR (c.location_id is ? AND c.remote = ?))'
            sql_params += [location_id, None, True]
        elif remote is True:
            sql += ''' AND ((c.location_id = ? AND c.remote = ?)
                OR (c.location_id is ? AND c.remote = ?))'''
            sql_params += [location_id, remote, None, True]
        elif remote is False:
            sql += ' AND (c.location_id = ? AND c.remote = ?)'
            sql_params += [location_id, remote]
    else:
        if remote is not None:
            sql += ' AND c.remote = ?'
            sql_params += [remote]

    if skills:
        num_skills = len(skills)
        for i in range(num_skills):
            skill, level = skills[i].split(',')
            if i == 0:
                sql += ' AND ('
            sql += '(s.skills_id = ? AND s.levels_id >= ?)'
            sql_params += [int(skill), int(level)]
            if i == num_skills - 1:
                sql += ')'
            else:
                sql += ' OR '

    data = database.read_query(sql, tuple(sql_params))
    if skills:
        data_dict = Counter(data)
        data = [el for el in data if data_dict[el] >= len(skills) - skills_threshold]

    return _return_ads_by_list(data)


def _return_ads_from_data(data: list[tuple]) -> list[CompanyAd]:
    """
    Returns list of company ads from data.\n
    :param data: list of row data from DB
    :return: list of company ads
    """
    result = []
    for row in data:
        c_ad = CompanyAd.from_query(*row[0:8], [])
        existing_ad = next((existing for existing in result if existing.id == c_ad.id), None)
        if existing_ad is None:
            ad_skills = Skill.from_query(row[8], row[9])
            c_ad.skills_levels.append(ad_skills)
            result.append(c_ad)
        else:
            ad_skills = Skill.from_query(row[8], row[9])
            existing_ad.skills_levels.append(ad_skills)
    return result


def _return_ads_by_list(ids: list[(int,)]) -> list[CompanyAd]:
    """
    Returns list of company ads from DB by given IDs. \n
    :param ids: list of company ads IDs
    :return: list of company ads
    """
    ids = [row[0] for row in ids]
    data = database.read_query(
        f'''SELECT c.*, s.skills_id, s.levels_id FROM company_ads c
        JOIN company_ads_has_skills s ON c.id = s.company_ads_id
        JOIN skills on skills.id = s.skills_id
        WHERE c.id IN ({','.join('?' * len(ids))}) AND skills.status = ?''',
        (*ids, SkillStatus.ACTIVE.value))
    result = []
    for row in data:
        c_ad = CompanyAd.from_query(*row[0:8], [])
        existing_ad = next((existing for existing in result if existing.id == c_ad.id), None)
        if existing_ad is None:
            ad_skills = Skill.from_query(row[8], row[9])
            c_ad.skills_levels.append(ad_skills)
            result.append(c_ad)
        else:
            ad_skills = Skill.from_query(row[8], row[9])
            existing_ad.skills_levels.append(ad_skills)
    return result


def return_ad_by_id(ad_id: int) -> CompanyAd:
    """
    Returns company ad with ID ad_id or raises HTTPException if ad
    does not exist.\n
    :param ad_id: ad ID
    :return: company ad
    """
    data = database.read_query(
        '''SELECT c.*, s.skills_id, s.levels_id FROM company_ads c
        JOIN company_ads_has_skills s ON c.id = s.company_ads_id
        JOIN skills ON skills.id = s.skills_id
        WHERE c.id = ? AND c.status != ? AND skills.status = ?''',
        (ad_id, CompAdsStatusAll.DELETED.value, SkillStatus.ACTIVE.value))
    if not data:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Company Ad with id {ad_id} does not exist')

    c_ad = None
    for row in data:
        if c_ad:
            c_ad.skills_levels.append(Skill.from_query(row[8], row[9]))
        else:
            c_ad = CompanyAd.from_query(*row[0:8], [])
            c_ad.skills_levels.append(Skill.from_query(row[8], row[9]))

    return c_ad


def check_company_ad_eligibility(company_ad_id: int) -> HTTPException | None:
    """
    Raises HTTPException if company ad status is not active or private.\n
    :param company_ad_id: ad ID
    :return: None
    """
    data = database.read_query('''SELECT 1 FROM company_ads WHERE id = ? AND
                               (status = ? OR status = ?)''',
                               (company_ad_id, CompAdsStatusAll.HIDDEN.value,
                                CompAdsStatusAll.DELETED.value))
    if data:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'Company ad with id {company_ad_id} not available.'
        )
    return None
