# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=invalid-name
# pylint: disable=unused-variable
# pylint: disable=redefined-builtin
# pylint: disable=redefined-outer-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-branches
# pylint: disable=too-many-locals

from mariadb import Error
from starlette import status
from fastapi import HTTPException

from src.services.users_service import get_user_by_id
from src.common.exceptions import NotFoundException, ForbiddenException, BadRequestException
from src.data import database
from src.data.database import update_query
from src.data.job_ad_models import JobAdCreate, JobAdRepr, JobAdWithMatches, JobAd, JobAdUpdate, \
    JobAdStatusAll, JobAdStatus
from src.data.user_models import UserData
from src.services.twitter_integration_service import create_tweet, get_job_ad_creator
from src.data.app_models import SkillStatus


def create(j_ad: JobAdCreate, user_id: int):
    """
    Creates new job ad in the database. \n
    :param j_ad: new job ad
    :param user_id: the id of the creator
    :return: the new ad that is saved
    """

    with database.get_connection() as conn:
        try:
            ad_id = database.process_query_transactional \
                ('''INSERT INTO job_ads(salary_min, salary_max, description, location_id, remote,
                status, companies_users_id) VALUES(?,?,?,?,?,?,?)''',
                 conn,
                 (j_ad.salary_min, j_ad.salary_max, j_ad.description, j_ad.location_id,
                  j_ad.remote, JobAdStatusAll.ACTIVE.value, user_id))

            res = [database.process_query_transactional(
                '''INSERT INTO job_ads_has_skills(job_ads_id, skills_id, levels_id) VALUES
                (?,(SELECT s.id FROM skills s WHERE s.id=? AND s.status=?),?)''',
                conn,
                (ad_id, skill.skill_id, SkillStatus.ACTIVE.value, skill.level_id))
                for skill in j_ad.skills]

            ad = JobAd.from_query(ad_id, j_ad.salary_min, j_ad.salary_max, j_ad.description,
                                  j_ad.location_id, j_ad.remote, JobAdStatus.ACTIVE.value,
                                  user_id, j_ad.skills)
            conn.commit()

            company_name = get_job_ad_creator(user_id)
            tweet = create_tweet(company_name, ad_id)

            return ad

        except Error as ex:
            conn.rollback()
            raise ex


def job_ad_update(ad_id: int, ad_update: JobAdUpdate, user_id: int):
    """
    Updates job ad in DB partially or fully or
    raises Error if user is not author of the ad or ad is already matched. \n
    :param ad_id: ID of the ad to be updated
    :param ad_update: updated fields of the ad
    :param user_id: user ID
    :return: job ad
    """
    ad = _get_active_job_ad_by_id_with_s(ad_id)
    if _validate_ad_owner(ad, user_id) is False:
        raise ForbiddenException('You are not the author of this ad and cannot update it.')
    if ad.status == JobAdStatus.MATCHED:
        raise BadRequestException('Ad has already been matched and cannot be edited.')
    if ad.status == JobAdStatusAll.DELETED:
        raise NotFoundException('Ad has been deleted.')
    updated_ad = ad.copy(update=ad_update.dict(exclude_none=True))
    ad_result = update_query(
        '''UPDATE job_ads SET salary_min=?, salary_max=?, description=?, remote=?, location_id=?
        WHERE id=?''',
        (updated_ad.salary_min, updated_ad.salary_max, updated_ad.description, updated_ad.remote,
         updated_ad.location_id, ad_id))

    if ad_update.skills:
        with database.get_connection() as conn:
            try:
                old_skills = database.process_query_transactional(
                    '''DELETE FROM job_ads_has_skills WHERE job_ads_id = ?''', conn, (ad_id,))
                res = [database.process_query_transactional(
                    '''INSERT INTO job_ads_has_skills(job_ads_id, skills_id, levels_id) VALUES
                    (?,(SELECT s.id FROM skills s WHERE s.id=? AND s.status=?),?)''',
                    conn,
                    (ad_id, skill_id[1], SkillStatus.ACTIVE.value, level_id[1]))
                    for skill_id, level_id in ad_update.skills]

                conn.commit()

            except Error as ex:
                conn.rollback()
                raise ex
    result = _get_active_job_ad_by_id_with_s(ad_id)
    return result


def get_job_ad_by_id_with_s_m(id, user: UserData) -> JobAdWithMatches:
    """
    Returns from DB the job ad with the relevant match requests if user is the author
     or admin, job ad without match requests if logged user is neither of those two.
     Raises NotFound Exception if job ad is deleted
    :param id: id of the job ad
    :param user: logged user
    :return: job ad

    """

    job_ad_data = database.read_query('SELECT * FROM job_ads WHERE id = ? AND status != ?',
                                      (id, JobAdStatusAll.DELETED.value))
    ad_repr = next((JobAdRepr.from_query(*row) for row in job_ad_data), None)

    if not ad_repr:
        raise NotFoundException(content='Ad not found')

    skills = _get_skills_by_ad_id(ad_repr.id)

    if _validate_ad_owner(ad_repr, user.id) is False and user.user_type_id != 3:
        matches = []
    else:
        matches = _get_matches_by_ad_id(ad_repr.id)

    result = JobAdWithMatches.from_query(id=ad_repr.id,
                                         salary_min=ad_repr.salary_min,
                                         salary_max=ad_repr.salary_max,
                                         description=ad_repr.description,
                                         location_id=ad_repr.location_id,
                                         remote=ad_repr.remote,
                                         status=ad_repr.status,
                                         companies_users_id=ad_repr.companies_users_id,
                                         skills=skills,
                                         matches=matches
                                         )
    return result


def get_ads_by_user_id(user_id: int, status: JobAdStatus | None = None) -> list[JobAd]:
    """
    Returns list of job ads for author by user_id
    :param user_id: user ID
    :return: list of company ads
    """
    if not status:
        data = database.read_query(
            '''SELECT * FROM job_ads_skills_flat WHERE companies_users_id = ? AND status != ?
            ORDER BY CASE status WHEN 'active' THEN 1 WHEN 'matched' THEN 2 END, id''',
            (user_id, JobAdStatusAll.DELETED.value)
        )
    else:
        data = database.read_query(
            'SELECT * FROM job_ads_skills_flat WHERE companies_users_id = ? AND status = ?',
            (user_id, status.value)
        )

    result = [JobAd.from_query(*row) for row in data]

    return result


def get_ads_by_user(user_id: int):
    """
    Returns list of job ads of user wtih ID. \n
    :param user_id: user ID
    :return: list of company ads
    """
    user = get_user_by_id(user_id)
    if user is None or user.deleted is True:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'User with id: {user_id} does not exist'
        )
    if user.user_type_id != 2:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'User with id: {user_id} is not registered as company'
        )
    return get_ads_by_user_id(user_id)


def _validate_ad_owner(ad: JobAdRepr | JobAd, user_id: int) -> bool:
    """
    Validates if the creator of the job ad is the same as a user by user_id
    :param ad: the referenced ad
    :param user_id: the user for whom we want to verify
    :return: bool
    """
    return ad.companies_users_id == user_id


def _get_skills_by_ad_id(ad_id):
    """
    Returns the skills that belong to a job ad by id
    :param ad_id: id of the job ad
    :return: list of skills
    """
    skills = database.read_query(
        '''SELECT js.skills_id, js.levels_id FROM job_ads_has_skills js
        JOIN skills s on s.id = js.skills_id
        WHERE js.job_ads_id = ? AND s.status != ?''', (ad_id, SkillStatus.DELETED.value))
    return skills


def _get_matches_by_ad_id(ad_id: int) -> list[tuple]:
    """
    Returns a list of matches for a job ad
    :param ad_id: the job id
    :return: list of tuples for the match requests
    """
    matches = database.read_query(
        '''SELECT id, matched, users_id  FROM match_requests
        WHERE job_ad_id = ?''', (ad_id,))
    return matches


def _get_active_job_ad_by_id_with_s(ad_id: int) -> JobAd | None:
    """
    Returns a job ad with the relevant skills
    :param ad_id: id of the job ad
    :return: job ad
    """
    data = database.read_query('''SELECT * FROM job_ads_skills_flat WHERE id = ? AND status = ?''',
                               (ad_id, JobAdStatusAll.ACTIVE.value))
    ad = next((JobAd.from_query(*row) for row in data), None)

    if not ad:
        raise NotFoundException(
            content='Add not found or already matched and could not be modified'
        )
    return ad


def check_job_ad_eligibility(job_ad_id: int) -> HTTPException | None:
    """
    Verifies whether a job ad is active
    :param job_ad_id: job ad is
    :return: None ir raises HTTP Exception with status code 403
    """
    data = database.read_query(
        '''SELECT 1 FROM job_ads WHERE id = ? AND status != "active"''',
        (job_ad_id,)
    )
    if data:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f'Job ad with id {job_ad_id} not available.'
        )
    return None


# TODO Consider refactoring

def _list_to_dict(l: list, sep: str = ':'):
    """
    Creates a dict with the skills with levels that are exported
    from the DB view as list of str \n
    :param l:
    :param sep: separator for the split
    :return: dictionary with the skills and their levels
    """

    return {int(s.split(sep)[0]): int(s.split(sep)[1]) for s in l}


def get_job_ads(salary_min: int | None, salary_max: int | None, salary_threshold: float,
                location_id: int | None, remote: bool | None,
                skills: list | None, skill_threshold: int = 0):
    """
    Returns a list of filtered job ads by given params \n
    :param salary_min: searched minimal salary
    :param salary_max: searched maximal salary
    :param salary_threshold: possibility to apply variation in salaries
     - [0, 0.99] applied on salary search params
    :param location_id: location ID
    :param remote: remote option
    :param skills:  '1,1' list of skill-level ids
    :param skill_threshold: skills threshold - integer
    :return: list of job ads
    """

    search_skills_dict = None
    if not skills == []:
        search_skills_dict = _list_to_dict(skills, ',')
    sql_params = [JobAdStatus.ACTIVE.value]
    main_select = '''SELECT * FROM job_ads_skills_flat WHERE status = ?'''

    if salary_min:
        main_select += ''' and salary_max >= ?'''
        sql_params.append(salary_min * (1 - salary_threshold))
    if salary_max:
        main_select += ''' and salary_min <= ?'''
        sql_params.append(salary_max * (1 + salary_threshold))

    if location_id:
        if remote is None:
            main_select += ' AND (location_id = ? OR (location_id is ? AND remote = ?))'
            sql_params += [location_id, None, True]
        elif remote is True:
            main_select += ''' AND ((location_id = ? AND remote = ?)
                            OR (location_id is ? AND remote = ?))'''
            sql_params += [location_id, remote, None, True]
        elif remote is False:
            main_select += ' AND (location_id = ? AND remote = ?)'
            sql_params += [location_id, remote]
    else:
        if remote is not None:
            main_select += ' AND remote = ?'
            sql_params += [remote]

    if search_skills_dict:
        s = '|'.join(str(el) for el in search_skills_dict.keys())
        main_select += f''' and skills_flat regexp "^{s}:"'''

    rows = database.read_query(main_select, tuple(sql_params))
    if not rows:
        return []
    res = rows

    if search_skills_dict:
        res = []
        for row in rows:
            match_count = 0
            row_skills = _list_to_dict(row[8].split(','))
            for skill_id in search_skills_dict.keys():
                if skill_id in row_skills and row_skills[skill_id] >= search_skills_dict[skill_id]:
                    match_count += 1
            if match_count >= len(search_skills_dict.keys()) - skill_threshold:
                res.append(row)

    return [JobAd.from_query(*row) for row in res]
