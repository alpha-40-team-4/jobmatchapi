# pylint: disable=missing-module-docstring
# pylint: disable=import-error

import os
from dotenv import load_dotenv
from mailjet_rest import Client
from src.common.exceptions import NotFoundException
from src.data import database

load_dotenv()


def _get_recipient_company_email_and_name(job_ad_id: int) -> list | NotFoundException:
    """
    Fetches the email and company_name of the recipient company.
    :param job_ad_id:
    :return: list | NotFoundException
    """
    data = database.read_query('''SELECT c.email, c.company_name FROM match_requests as mr
                                    JOIN job_ads as ja
                                    ON mr.job_ad_id = ja.id
                                    JOIN companies as c
                                    ON ja.companies_users_id = c.users_id
                                    WHERE mr.job_ad_id = ?''', (job_ad_id,))
    if not data:
        return NotFoundException(f'Author of job ad with id: {job_ad_id} not found')
    return data


def _get_recipient_professional_email_and_name(company_ad_id: int) -> list | NotFoundException:
    """
    Fetches the email and professional_name of the recipient professional.
    :param company_ad_id:
    :return: list | NotFoundException
    """
    data = database.read_query('''SELECT p.email, p.first_name FROM match_requests as mr
                                    JOIN company_ads as ca
                                    ON mr.company_ad_id = ca.id
                                    JOIN professionals as p
                                    ON ca.professionals_users_id = p.users_id
                                    WHERE mr.company_ad_id = ?''', (company_ad_id,))
    if not data:
        return NotFoundException(f'Author of company ad with id: {company_ad_id} not found')
    return data


def send_match_email_to_company(job_ad_id: int):
    """
    Announces a received match_request by a system email to the recipient company.
    :param job_ad_id:
    :return: None
    """
    company_data = _get_recipient_company_email_and_name(job_ad_id)

    api_key = os.getenv('API_KEY')
    api_secret = os.getenv('API_SECRET')

    mailjet = Client(auth=(api_key, api_secret), version='v3.1')

    data = {
        'Messages': [
            {
                "From": {
                    "Email": "plamen.gunchev.a40@learn.telerikacademy.com",
                    "Name": "[System] JobMatchAPI"
                },
                "To": [
                    {
                        "Email": f"{company_data[0][0]}",
                        "Name": f"{company_data[0][1]}"
                    }
                ],
                "Subject": "You've received a new match request!",
                "TextPart": "JobMatchAPI System",
                "HTMLPart": f"<h3>Dear {company_data[0][1]}, a professional matched your"
                            f"job ad posting. Feel free to "
                            f"check it here: <a href='https://www.fakelink.com/'>Check your"
                            f"posting!</a></h3><br "
                            f"/>Yours,<br />The JobMatchAPI Team",
                "CustomID": "AppGettingStartedTest"
            }
        ]
    }
    result = mailjet.send.create(data=data)
    print(result.status_code)
    print(result.json())


def send_match_email_to_professional(company_ad_id: int):
    """
    Announces a received match_request by a system email to the recipient professional.
    :param company_ad_id:
    :return: None
    """
    professional_data = _get_recipient_professional_email_and_name(company_ad_id)

    api_key = os.getenv('API_KEY')
    api_secret = os.getenv('API_SECRET')

    mailjet = Client(auth=(api_key, api_secret), version='v3.1')

    data = {
        'Messages': [
            {
                "From": {
                    "Email": "plamen.gunchev.a40@learn.telerikacademy.com",
                    "Name": "[System] JobMatchAPI"
                },
                "To": [
                    {
                        "Email": f"{professional_data[0][0]}",
                        "Name": f"{professional_data[0][1]}"
                    }
                ],
                "Subject": "You've received a new match request!",
                "TextPart": "JobMatchAPI System",
                "HTMLPart": f"<h3>Dear {professional_data[0][1]}, "
                            f"a company have sent a match request to your company ad posting."
                            f"Feel free to check it here:"
                            f"<a href='https://www.fakelink.com/'>Check your "
                            f"posting!</a></h3> "
                            f"<br />Yours,<br />The JobMatchAPI Team",
                "CustomID": "AppGettingStartedTest"
            }
        ]
    }
    result = mailjet.send.create(data=data)
    print(result.status_code)
    print(result.json())
