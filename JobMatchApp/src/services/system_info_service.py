# pylint: disable=missing-module-docstring
# pylint: disable=import-error

from mariadb import Error
from src.data import database
from src.data.app_models import Level, LocationInfo, SkillInfo, SkillStatus, LocationStatus
from src.data.user_models import UserData


def get_locations(status: str):
    """
    Returns list of locations filtered by status.\n
    :param status: status [active, pending]
    :return: list of locations
    """
    data = database.read_query(
        'SELECT id, city FROM locations WHERE status = ? ORDER BY id',
        (status,))
    return [LocationInfo.from_query(*row) for row in data]


def get_skills(status: str):
    """
    Returns list of skills filtered by status.\n
    :param status: status [active, pending]
    :return: list of skills
    """
    data = database.read_query(
        'SELECT id, name from skills WHERE status = ? ORDER BY id', (status,)
    )
    return [SkillInfo.from_query(*row) for row in data]


def ad_new_skills(new_skills: list[str], current_user: UserData) -> str | Error:
    """
    Adds a new skill in DB with status 'active' if
    user is admin or 'pending' if company
    or professional.\n
    :param new_skills: list of skill names
    :param current_user: user who adds skills
    :return: str | Error
    """
    if current_user.user_type_id == 3:
        status = SkillStatus.ACTIVE.value
    else:
        status = SkillStatus.PENDING.value

    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                'INSERT INTO skills(name, status) VALUES (?,?)', conn,
                (skill, status)) for skill in new_skills]
            conn.commit()
        except Error as ex:
            conn.rollback()
            raise ex

    return f'Skills {new_skills} added'


def approve_pending_skills(skills_ids: list[int]) -> str | Error:
    """
    Bulk approves pending skills by IDs.\n
    :param skills_ids: list of skills IDs
    :return: str | Error
    """
    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                'UPDATE skills SET status = ? WHERE id = ? and status = ?', conn,
                (SkillStatus.ACTIVE.value, s_id, SkillStatus.PENDING.value)) for s_id in skills_ids]
            conn.commit()
        except Error as ex:
            conn.rollback()
            raise ex

    return f'Skills {skills_ids} approved'


def delete_skills_by_ids(skills_ids: list[int]) -> str | Error:
    """
    Bulk delete skills by IDs, only, if they are not already in use.\n
    :param skills_ids: list of skills IDs
    :return: str | Error
    """
    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                '''UPDATE skills SET status = ? WHERE id = ?
                AND id NOT IN (SELECT skills_id FROM company_ads_has_skills)
                AND id NOT IN (SELECT skills_id FROM job_ads_has_skills)''',
                conn, (SkillStatus.DELETED.value, s_id)) for s_id in skills_ids]

            conn.commit()
        except Error as ex:
            conn.rollback()
            raise ex

    return f'Skills {skills_ids} deleted'


def get_levels() -> list[Level]:
    """
    Returns a list of all level IDs and names from DB.\n
    :return: list of levels
    """
    data = database.read_query('SELECT id, level from levels ORDER BY id')
    return [Level.from_query(*row) for row in data]


def ad_new_location(new_locations: list[str], current_user: UserData):
    """
    Adds a new locations in DB with status 'active' if user is admin or 'pending' if company
    or professional.\n
    :param new_locations: list of locations (cities)
    :param current_user: user data
    :return: str / Error
    """
    if current_user.user_type_id == 3:
        status = LocationStatus.ACTIVE.value
    else:
        status = LocationStatus.PENDING.value

    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                'INSERT INTO locations(city, status) VALUES (?,?)',
                conn, (location, status)) for location in new_locations]

            conn.commit()
            return f'Location/-s {", ".join(new_locations)} added as {status}.'
        except Error as ex:
            conn.rollback()
            raise ex


def approve_pending_locations(locations_ids: list[int]):
    """
    Bulk approves pending locations by IDs.\n
    :param locations_ids: list of locations IDs
    :return: str | Error
    """
    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                'UPDATE locations SET status = ? WHERE id = ? and status = ?', conn,
                (LocationStatus.ACTIVE.value, l_id, LocationStatus.PENDING.value))
                for l_id in locations_ids]

            conn.commit()
            return f'Location/-s {", ".join([str(l_id) for l_id in locations_ids])} approved.'
        except Error as ex:
            conn.rollback()
            raise ex


def delete_locations_by_ids(locations_ids: list[int]):
    """
    Bulk delete locations by IDs, only if they are not already in use.\n
    :param locations_ids: list of locations IDs
    :return: str | Error
    """
    with database.get_connection() as conn:
        try:
            data = [database.process_query_transactional(
                '''UPDATE locations SET status = ? WHERE id = ?
                AND id NOT IN (SELECT distinct location_id
                    FROM company_ads where location_id is not null)
                AND id NOT IN (SELECT distinct location_id
                    FROM job_ads where location_id is not null)
                AND id NOT IN (SELECT distinct location_id
                    FROM professionals where location_id is not null)
                AND id NOT IN (SELECT distinct location_id
                    FROM companies where location_id is not null)''',
                conn, (LocationStatus.DELETED.value, id)) for id in locations_ids]
            conn.commit()
            return f'Location/-s {", ".join([str(id) for id in locations_ids])} deleted.'
        except Error as ex:
            conn.rollback()
            raise ex
