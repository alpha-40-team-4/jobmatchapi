# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=import-error
# pylint: disable=no-name-in-module
# pylint: disable=too-few-public-methods
# pylint: disable=raise-missing-from

from datetime import datetime, timedelta
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from src.data.user_models import UserData
from src.services import users_service


# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 180


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def encode_password(password):
    """
    Jwt encoding of user password.\n
    :param password: str
    :return: encoded password
    """
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    """
    Verifies users password matches hashed password.\n
    :param plain_password: str
    :param hashed_password: str
    :return: None
    """
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(username: str, password: str):
    """
    Returns user or False if username doesn't exist or password doesn't match. \n
    :param username: str
    :param password: str
    :return: user data
    """
    user = users_service.get_user_by_username(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    """
    Returns jwt encoded token valid 15 minutes if no expires delta provided. \n
    :param data: dict
    :param expires_delta: token validity
    :return: token
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_current_user(token: str = Depends(oauth2_scheme)):
    """Returns current user if token is valid or raises Error if token is not valid,
    user is not approved by admin, blocked or does not exist"""
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError as ex:
        raise credentials_exception from ex
    user = users_service.get_user_by_username(username=token_data.username)
    if user is None:
        raise credentials_exception
    if user.approved is False:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='User registration must be approved by admin'
            )
    if user.blocked is True:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='User account is blocked'
            )
    if user.deleted is True:
        raise credentials_exception
    return user


def get_non_logged_user(token: str = Depends(oauth2_scheme)):
    """Raises Error if token is valid"""
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        token_data = TokenData(username=username)
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='Error'
            )
    user = users_service.get_user_by_username(username=token_data.username)
    if user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='User is already registered'
        )


def get_current_active_professional(current_user: UserData = Depends(get_current_user)):
    """Returns current user if professional or raises Error if not"""
    if current_user.user_type_id != 1:
        raise HTTPException(
            status_code=403,
            detail="User should be registered as professional"
            )
    return current_user


def get_current_active_professional_or_admin(current_user: UserData = Depends(get_current_user)):
    """Returns current user if professional or admin, or raises Error if company"""
    if current_user.user_type_id == 2:
        raise HTTPException(
            status_code=403,
            detail="User should be registered as professional or admin"
            )
    return current_user


def get_current_active_company(current_user: UserData = Depends(get_current_user)):
    """Returns current user if company, or raises Error if not"""
    if current_user.user_type_id != 2:
        raise HTTPException(
            status_code=400,
            detail="User should be registered as company"
            )
    return current_user


def get_current_active_company_or_admin(current_user: UserData = Depends(get_current_user)):
    """Returns current user if company or admin, or raises Error if professional"""
    if current_user.user_type_id == 1:
        raise HTTPException(
            status_code=400,
            detail="User should be registered as company or admin"
            )
    return current_user


def get_current_admin(current_user: UserData = Depends(get_current_user)):
    """Returns current user if admin, or raises Error if not"""
    if current_user.user_type_id != 3:
        raise HTTPException(
            status_code=400,
            detail="User should be admin"
            )
    return current_user
