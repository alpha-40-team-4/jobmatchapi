# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=unused-variable


class BadRequestException(Exception):
    def __init__(self, content=''):
        super().__init__(content)
        status_code = 400


class ForbiddenException(Exception):
    def __init__(self, content=''):
        super().__init__(content)
        status_code = 403


class NotFoundException(Exception):
    def __init__(self, content=''):
        super().__init__(content)
        status_code = 404
