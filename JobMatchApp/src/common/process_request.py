# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring


from typing import Any, Callable, Union
from fastapi import HTTPException, Response
from pydantic import ValidationError
from mariadb import OperationalError, IntegrityError, DataError
from src.common.exceptions import BadRequestException
from src.common.exceptions import ForbiddenException, NotFoundException
from src.common.responses import BadRequest, Forbidden, NotFound, ErrorResponse


def process_request(execute_fn: Callable) -> Union[Any, Response]:
    try:
        return execute_fn()
    except HTTPException as ex:
        return ErrorResponse(
            status_code=ex.status_code,
            content=ex.detail)

    except (IntegrityError, DataError) as ex:
        return BadRequest(content=str(ex))

    except OperationalError as ex:
        return BadRequest(content=str(ex))

    except ValidationError as ex:
        return BadRequest(content=str(ex))

    except BadRequestException as ex:
        return BadRequest(content=str(ex))

    except ForbiddenException as ex:
        return Forbidden(content=str(ex))

    except NotFoundException as ex:
        return NotFound(content=str(ex))
