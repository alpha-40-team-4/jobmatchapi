# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring

from typing import Any
from fastapi import Response
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder


class BadRequest(Response):
    def __init__(self, content=''):
        super().__init__(status_code=400, content=content)


class Forbidden(Response):
    def __init__(self, content=''):
        super().__init__(status_code=403, content=content)


class NotFound(Response):
    def __init__(self, content=''):
        super().__init__(status_code=404, content=content)


class UnprocessableEntity(Response):
    def __init__(self, content=''):
        super().__init__(status_code=422, content=content)


class ErrorResponse(JSONResponse):

    def render(self, content: Any) -> bytes:

        return super().render(jsonable_encoder({"detail": {"error": str(content)}}))
