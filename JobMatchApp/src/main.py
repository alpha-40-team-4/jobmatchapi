# pylint: disable=missing-module-docstring
# pylint: disable=import-error

from datetime import timedelta
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
import uvicorn

from common.security import Token, authenticate_user, ACCESS_TOKEN_EXPIRE_MINUTES,\
    create_access_token
from routers.company_ad import company_ad_router
from routers.user import user_router
from routers.user_info import user_info_router
from routers.job_ad import job_ad_router
from routers.match_request import match_request_router
from routers.admin import admin_router
from routers.system_info import system_info_router

tags_metadata = [
    {
        "name": "registration",
        "description": "Register user as a **professional** or a **company**",
    },
    {
        "name": "users",
        "description": 'View or update profile info',
    },
    {
        "name": "company ads",
        "description": """Create or update own company ad, view own company ad/ads
        (**for professionals only**). Search company ads by salary, location, skills
        (**for admins and companies only**)""",
    },
    {
        "name": "job ads",
        "description": "Create or update own job ad, view own job ads (**for companies only**).",
    },
    {
        "name": "match requests",
        "description": "",
    },
    {
        "name": "admins",
        "description": "",
    },
    {
        "name": "system",
        "description": """View available locations, skills/requirements;
        add, approve, delete skills/requirements"""
    },
    {
        "name": "login"
    }
]

DESCRIPTION = '''
**JobMatchAPI is a RESTful API that provides the functionality for a web-based job board. 
Companies and professionals can register and post job listings on the platform. 
Those listings can be filtered and searched based on a number of criterion - 
location, salary range, skills & requirements, etc.**

'''

app = FastAPI(title='TopTeam JobMatchApp',
              description=DESCRIPTION,
              openapi_tags=tags_metadata,
              version='v1.0'
              )

app.include_router(user_router)
app.include_router(user_info_router)
app.include_router(company_ad_router)
app.include_router(job_ad_router)
app.include_router(match_request_router)
app.include_router(admin_router)
app.include_router(system_info_router)


@app.post("/token", response_model=Token, tags=['login'])
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if user.blocked is True:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='User account is blocked'
        )
    if user.deleted is True:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='User account is deleted'
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username},
        expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8000)
