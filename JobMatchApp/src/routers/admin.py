# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring

from fastapi import APIRouter, Depends, Query
from src.common.security import get_current_admin
from src.common.process_request import process_request
from src.services.users_service import del_job_ad, delete_users, delete_comp_ad
from src.services.users_service import confirm_user_registration, get_blocked_users
from src.services.users_service import update_blocked_unblocked, get_pending_registrations


admin_router = APIRouter(prefix='/admins', tags=['admins'],
                         dependencies=[Depends(get_current_admin)])


@admin_router.get('/pending_users', summary='View users with pending registrations',
                  description='Available for **admins only**')
def get_unapproved_registrations():

    def _get_unapproved_registrations():
        users = get_pending_registrations()
        return users

    return process_request(_get_unapproved_registrations)


@admin_router.patch('/pending_users', summary='Approve pending registrations by user ids',
                    description='Available for **admins only**')
def approve_registration(user_ids: list[int] = Query()):

    def _approve_registration():
        result = confirm_user_registration(user_ids)
        return result

    return process_request(_approve_registration)


@admin_router.get('/blocked_users', summary='View blocked users',
                  description='Available for **admins only**')
def get_blocked():

    def _get_blocked():
        blocked_users = get_blocked_users()
        return  blocked_users

    return process_request(_get_blocked)


@admin_router.patch('/blocked_users', summary='Block/unblock users by ids',
                    description='Available for **admins only**')
def set_blocked(blocked: bool, user_ids: list[int] = Query()):

    def _set_blocked():
        result = update_blocked_unblocked(blocked, user_ids)
        return  result

    return process_request(_set_blocked)


@admin_router.delete('/users', summary='Delete users by ids',
                     description='Available for **admins only**')
def delete(user_ids: list[int] = Query()):

    def _delete():
        result = delete_users(user_ids)
        return result

    return process_request(_delete)


@admin_router.delete('/company_ads', summary='Delete company ad',
                     description='Available for **admins only**')
def delete_company_ad(company_ad_id: int):

    def _delete_company_ad():
        result = delete_comp_ad(company_ad_id)
        return result

    return process_request(_delete_company_ad)


@admin_router.delete('/job_ads', summary='Delete job ad',
                     description='Available for **admins only**')
def delete_job_ad(job_ad_id: int):

    def _delete_job_ad():
        result = del_job_ad(job_ad_id)
        return result

    return process_request(_delete_job_ad)
