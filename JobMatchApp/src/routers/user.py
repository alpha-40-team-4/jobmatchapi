# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring

from fastapi import APIRouter, Depends
from src.common.process_request import process_request
from src.data.user_models import ProfessionalRegData, CompanyRegData
from src.services import users_service
from src.common.security import get_non_logged_user


user_router = APIRouter(prefix='/users',
                        dependencies=[Depends(get_non_logged_user)],
                        tags=['registration'])


@user_router.post('/professionals')
def register_professional(data: ProfessionalRegData):

    def _register_professional():
        result = users_service.register_professional_wrapper(data)
        return result

    return process_request(_register_professional)


@user_router.post('/companies')
def register_company(data: CompanyRegData):

    def _register_company():
        result = users_service.register_company_wrapper(data)
        return result

    return process_request(_register_company)
