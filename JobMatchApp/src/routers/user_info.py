# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring

from fastapi import APIRouter, Depends
from src.data.user_models import UserData
from src.data.info_models import CompanyInfoUpdate, ProfInfoUpdate
from src.common.security import get_current_user, get_current_active_company, \
    get_current_active_professional, get_current_admin
from src.common.process_request import process_request
from src.services.info_service import update_company, update_prof, search_companies, \
    search_professionals, get_user_info_by_userdata, get_user_info_by_id
from src.services.users_service import get_company_by_id, get_professional_by_id, \
    get_sent_requests_by_prof_id, get_sent_requests_by_company_id

user_info_router = APIRouter(prefix='/info', tags=['users'])


@user_info_router.get('/', summary='View own user info')
def get_user_info(current_user: UserData = Depends(get_current_user)):
    def _get_user_info():
        user = get_user_info_by_userdata(current_user)
        return user

    return process_request(_get_user_info)


@user_info_router.get('/admin/{user_id}', dependencies=[Depends(get_current_admin)],
                      summary='View user info', description='Available for **admins** only')
def get_user_information(user_id: int):

    def _get_user_information():
        result = get_user_info_by_id(user_id)
        return result

    return process_request(_get_user_information)


@user_info_router.patch('/companies', summary='Update company info',
                        description='Available for **companies only**')
def update_company_info(company_info_update: CompanyInfoUpdate,
                        current_user: UserData = Depends(get_current_active_company)):

    def _update_company_info():
        company = get_company_by_id(current_user.id)
        updated_company_info = update_company(company, company_info_update)
        return updated_company_info

    return process_request(_update_company_info)


@user_info_router.patch('/professionals', summary='Update professional info',
                        description='Available for **professionals only**')
def update_professional_info(prof_info_update: ProfInfoUpdate,
                             current_user: UserData = Depends(get_current_active_professional)):

    def _update_professional_info():
        professional = get_professional_by_id(current_user.id)
        updated_professional_info = update_prof(professional, prof_info_update)
        return updated_professional_info

    return process_request(_update_professional_info)


@user_info_router.get('/companies', dependencies=[Depends(get_current_user)],
                      summary='Search for companies')
def get_companies(name: str | None = None, description: str | None = None,
                  location_id: int | None = None):

    def _get_companies():
        companies = search_companies(name, description, location_id)
        return companies

    return process_request(_get_companies)


@user_info_router.get('/professionals', dependencies=[Depends(get_current_user)],
                      summary='Search for professionals')
def get_professionals(first_name: str | None = None, last_name: str | None = None,
                      summary: str | None = None, location_id: int | None = None):

    def _get_professionals():
        professionals = search_professionals(first_name, last_name, summary, location_id)
        return professionals

    return process_request(_get_professionals)


@user_info_router.get('/professionals/requests',
                      dependencies=[Depends(get_current_active_professional)],
                      summary='View the match requests that were sent with their status',
                      description='Available for **professionals only**')
def get_sent_match_requests_as_prof(
        current_user: UserData = Depends(get_current_active_professional)):

    def _get_sent_match_requests_as_prof():
        job_ad_requests = get_sent_requests_by_prof_id(current_user.id)
        return job_ad_requests

    return process_request(_get_sent_match_requests_as_prof)


@user_info_router.get('/companies/requests', dependencies=[Depends(get_current_active_company)],
                      summary='View the match requests that were sent with their status',
                      description='Available for **companies only**')
def get_sent_match_requests_as_company(
        current_user: UserData = Depends(get_current_active_company)):

    def _get_sent_match_requests_as_company():
        company_ad_requests = get_sent_requests_by_company_id(current_user.id)
        return company_ad_requests

    return process_request(_get_sent_match_requests_as_company)
