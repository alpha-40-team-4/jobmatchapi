# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments

from fastapi import APIRouter, Depends, Query
from src.common.process_request import process_request
from src.common.security import get_current_active_company, \
    get_current_active_professional_or_admin, get_current_admin, \
    get_current_user
from src.data.user_models import UserData
from src.data.job_ad_models import JobAd, JobAdCreate, JobAdWithMatches, \
    JobAdUpdate, JobAdStatus
from src.services import job_ad_service
from src.services.job_ad_service import get_ads_by_user_id, job_ad_update, \
    get_job_ads, get_ads_by_user

job_ad_router = APIRouter(prefix='/job_ads', tags=['job ads'])


@job_ad_router.post('/', response_model=JobAd, summary='Create new job ad',
                    description='Available for **companies only**', status_code=201)
def create_job_ad(ad: JobAdCreate, current_user: UserData = Depends(get_current_active_company, )):

    def _create_job_ad():
        job_ad = job_ad_service.create(ad, current_user.id)
        return job_ad

    return process_request(_create_job_ad)


@job_ad_router.get('/own', response_model=list[JobAd],
                   summary='View all own job ads - Active, Matched or All',
                   description='Available for **companies only**')
def view_own_job_ads(status: JobAdStatus | None = None,
                     current_user: UserData = Depends(get_current_active_company)):

    def _get_by_user_id():
        result = get_ads_by_user_id(current_user.id, status)
        return result

    return process_request(_get_by_user_id)


@job_ad_router.get('/admin/{user_id}', dependencies=[Depends(get_current_admin)],
                   response_model=list[JobAd],
                   summary='View all job ads by user id',
                   description='Available for **admins only**')
def view_ads_by_user(user_id: int):

    def _view_ads_by_user():
        result = get_ads_by_user(user_id)
        return result

    return process_request(_view_ads_by_user)


@job_ad_router.get('/all', dependencies=[Depends(get_current_active_professional_or_admin)],
                   response_model=list[JobAd], summary='View job ads',
                   description='Search through job ads by salary, location and/or skills. '
                               'Available for **admins** and **professionals**')
def search_ads(min_salary: int | None = None, max_salary: int | None = None,
               salary_threshold: float = Query(default=0, ge=0, le=0.99),
               location_id: int = None, remote: bool | None = None,
               skills: list = Query(default=[]), skills_threshold: int = 0):

    def _search_job_ads():
        result = get_job_ads(min_salary, max_salary, salary_threshold, location_id,
                             remote, skills, skills_threshold)
        return result

    return process_request(_search_job_ads)


@job_ad_router.patch('/{ad_id}', summary='Update job ad',
                     description='Updates job_ad parameters.'
                                 'Available for **companies** only')
def update_ad(ad_id: int,
              new_ad: JobAdUpdate,
              current_user: UserData = Depends(get_current_active_company)):

    def _update_ad():
        job_ad = job_ad_update(ad_id, new_ad, current_user.id)
        return job_ad

    return process_request(_update_ad)


@job_ad_router.get('/{ad_id}', response_model=JobAdWithMatches, summary='View job ad by id',
                   description='View job ad with match requests included if the requester is '
                               'the **author** or **admin** and without match requests if '
                               'requester is not the author.')
def view_by_job_ad_id(ad_id: int, current_user: UserData = Depends(get_current_user)):

    def _view_add():
        job_ad = job_ad_service.get_job_ad_by_id_with_s_m(ad_id, current_user)
        return job_ad

    return process_request(_view_add)
