# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring

from fastapi import APIRouter, Depends
from src.data.user_models import UserData
from src.common.security import get_current_active_company, \
    get_current_active_professional
from src.common.process_request import process_request
from src.services import match_request_service as mrs

match_request_router = APIRouter(prefix='/match_requests',
                                 tags=['match requests'])


@match_request_router.post('/companies/{company_ad_id}',
                           summary='Send match request to a company ad',
                           description='Available for **companies only**')
def send_match_request_as_company(company_ad_id: int,
                                  current_user:
                                  UserData = Depends(get_current_active_company)):

    def _send_match_request_as_company():
        result = mrs.create_company_ad_request_wrapper(current_user.id, company_ad_id)
        return result

    return process_request(_send_match_request_as_company)


@match_request_router.post('/professionals/{job_ad_id}',
                           summary='Send match request to a job ad',
                           description='Available for **professionals only**')
def send_match_request_as_professional(job_ad_id: int,
                                       current_user:
                                       UserData = Depends(get_current_active_professional)):

    def _send_match_request_as_professional():
        result = mrs.create_job_ad_request_wrapper(current_user.id, job_ad_id)
        return result

    return process_request(_send_match_request_as_professional)


@match_request_router.patch('/companies/{match_request_id}',
                            summary='Match a request from a professional to a job ad',
                            description='Available for **companies only**')
def match_professional_request(match_request_id: int,
                               company: UserData = Depends(get_current_active_company)):

    def _match_professional_request():
        result = mrs.match_with_professional_wrapper(match_request_id, company.id)
        return result

    return process_request(_match_professional_request)


@match_request_router.patch('professionals/{match_request_id}',
                            summary='Match a request from a company to a company ad',
                            description='Available for **professionals only**')
def match_company_request(match_request_id: int,
                          professional: UserData = Depends(get_current_active_professional)):

    def _match_company_request():
        result = mrs.match_with_company_wrapper(match_request_id, professional.id)
        return result

    return process_request(_match_company_request)
