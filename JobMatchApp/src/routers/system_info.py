# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring

from fastapi import APIRouter, Depends, Query
from src.data.app_models import LocationInfo, SkillInfo, Level, SkillStatus, LocationStatus
from src.common.security import get_current_user, get_current_admin
from src.common.process_request import process_request
from src.services.system_info_service import get_locations, get_skills, get_levels, ad_new_skills, \
    approve_pending_skills, delete_skills_by_ids, ad_new_location, approve_pending_locations, \
    delete_locations_by_ids

system_info_router = APIRouter(prefix='/system_info', tags=['system'])


@system_info_router.get('/locations', summary='View available locations',
                        dependencies=[Depends(get_current_user)],
                        response_model=list[LocationInfo])
def get_locations_info():

    def _get_locations_info():
        locations = get_locations(LocationStatus.ACTIVE.value)
        return locations

    return process_request(_get_locations_info)


@system_info_router.post('/locations', summary='Add location',
                         description="Add as 'active' if **admin** or 'pending' "
                                     "if company or professional")
def add_location(new_locations: list[str] = Query(), current_user=Depends(get_current_user)):

    def _add_location():
        result = ad_new_location(new_locations, current_user)
        return result

    return process_request(_add_location)


@system_info_router.patch('/locations', dependencies=[Depends(get_current_admin)],
                          summary='Approve pending locations',
                          description='Available for **admins** only')
def approve_location(locations_ids: list[int] = Query()):

    def _approve_locations():
        result = approve_pending_locations(locations_ids)
        return result

    return process_request(_approve_locations)


@system_info_router.delete('/locations', dependencies=[Depends(get_current_admin)],
                           summary='Delete locations', description='Available for **admins** only')
def delete_locations(locations_ids: list[int] = Query()):

    def _delete_locations():
        result = delete_locations_by_ids(locations_ids)
        return result

    return process_request(_delete_locations)


@system_info_router.get('/locations/pending',
                        summary='View pending locations',
                        description='Available for **admins** only',
                        dependencies=[Depends(get_current_admin)],
                        response_model=list[LocationInfo])
def get_pending_locations_info():

    def _get_pending_locations_info():
        pending_locations = get_locations(LocationStatus.PENDING.value)
        return pending_locations

    return process_request(_get_pending_locations_info)


@system_info_router.get('/skills_requirements', summary='View available skills/requirements',
                        dependencies=[Depends(get_current_user)], response_model=list[SkillInfo])
def get_skills_info():

    def _get_skills_info():
        skills = get_skills(SkillStatus.ACTIVE.value)
        return skills

    return process_request(_get_skills_info)


@system_info_router.get('/skills_requirements/pending',
                        summary='View pending skills/requirements',
                        description='Available for **admins** only',
                        dependencies=[Depends(get_current_admin)], response_model=list[SkillInfo])
def get_pending_skills_info():

    def _get_pending_skills_info():
        pending_skills = get_skills(SkillStatus.PENDING.value)
        return pending_skills

    return process_request(_get_pending_skills_info)


@system_info_router.post('/skills_requirements', summary='Add skills/requirements',
                         description="Add as 'active' if **admin** or 'pending'"
                                     " if company or professional")
def add_skills(new_skills: list[str] = Query(), current_user=Depends(get_current_user)):

    def _add_skills():
        result = ad_new_skills(new_skills, current_user)
        return result

    return process_request(_add_skills)


@system_info_router.patch('/skills_requirements', dependencies=[Depends(get_current_admin)],
                          summary='Approve pending skills/requirements',
                          description='Available for **admins** only')
def approve_skills(skills_ids: list[int] = Query()):

    def _approve_skills():
        result = approve_pending_skills(skills_ids)
        return result

    return process_request(_approve_skills)


@system_info_router.delete('/skills_requirements', dependencies=[Depends(get_current_admin)],
                           summary='Delete unused skills/requirements',
                           description='Available for **admins** only')
def delete_skills(skills_ids: list[int] = Query()):

    def _delete_skills():
        result = delete_skills_by_ids(skills_ids)
        return result

    return process_request(_delete_skills)


@system_info_router.get('/skills_requirements_levels', dependencies=[Depends(get_current_user)],
                        summary='View available skills/requirements levels',
                        response_model=list[Level])
def get_levels_info():

    def _get_levels_info():
        levels = get_levels()
        return levels

    return process_request(_get_levels_info)
