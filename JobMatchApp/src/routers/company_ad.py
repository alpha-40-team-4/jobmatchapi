# pylint: disable=missing-module-docstring
# pylint: disable=import-error
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-arguments

from typing import Union
from fastapi import APIRouter, Depends, Query
from src.common.process_request import process_request
from src.common.security import get_current_active_company_or_admin, \
    get_current_active_professional, get_current_user, get_current_admin
from src.data.company_ad_models import CompanyAdCreate, CompanyAd, CompanyAdWithRequests, \
    CompanyAdUpdate
from src.data.user_models import UserData
from src.services.company_ad_service import create, get_ad_by_id, get_ads_by_user_id, update, \
    get_ads_by_user_id_admin, search_comp_ads


company_ad_router = APIRouter(prefix='/company_ads', tags=['company ads'])


@company_ad_router.post('/', response_model=CompanyAd, status_code=201,
                        summary='Create new company ad',
                        description='Available for **professionals only**')
def create_company_ad(comp_ad: CompanyAdCreate,
                      current_user: UserData = Depends(get_current_active_professional)):

    def _create_company_ad():
        company_ad = create(comp_ad, current_user.id)
        return company_ad

    return process_request(_create_company_ad)


@company_ad_router.get('/own', response_model=list[CompanyAd], summary='View all own company ads',
                       description='Available for **professionals only**')
def view_ads_by_user_id(current_user: UserData = Depends(get_current_active_professional)):

    def _view_ads_by_user_id():
        company_ads = get_ads_by_user_id(current_user.id)
        return company_ads

    return process_request(_view_ads_by_user_id)


@company_ad_router.get('/admin/{user_id}', dependencies=[Depends(get_current_admin)],
                       response_model=list[CompanyAd], summary='View all company ads by user id',
                       description='Available for **admins only**')
def view_ads_by_user(user_id: int):

    def _view_ads_by_user():
        company_ads = get_ads_by_user_id_admin(user_id)
        return company_ads

    return process_request(_view_ads_by_user)


@company_ad_router.get('/all', dependencies=[Depends(get_current_active_company_or_admin)],
                       response_model=list[CompanyAd], summary='View company ads',
                       description='''Search through company ads by salary, location and/or skills.
                                   Available for **admins** and **companies**''')
def search_ads(min_salary: int | None = Query(default=None, ge=0),
               max_salary: int | None = Query(default=None, ge=0),
               salary_threshold: float = Query(default=0, ge=0, le=0.99),
               location_id: int = None,
               remote: bool | None = None,
               skills: list = Query(default=[], example=['1,1', '2,2']),
               skills_threshold: int = 0):

    def _search_ads():
        company_ads = search_comp_ads(min_salary, max_salary, salary_threshold, location_id, remote,
                                      skills, skills_threshold)
        return company_ads

    return process_request(_search_ads)


@company_ad_router.get('/{ad_id}', response_model=Union[CompanyAdWithRequests, CompanyAd],
                       summary='View company ad by id',
                       description='''Company ad is returned with a list of match requests
                       to its **author** or **admin** and without it to companies''')
def view_ad_by_ad_id(ad_id: int, current_user: UserData = Depends(get_current_user)):

    def _view_ad_by_ad_id():
        company_ad = get_ad_by_id(ad_id, current_user)
        return company_ad

    return process_request(_view_ad_by_ad_id)


@company_ad_router.patch('/{ad_id}', response_model=CompanyAd, summary='Update company ad',
                         description='''Accepts only fields that should be updated. Listed skills
                                    are added, not updated. Available for **professionals** only''')
def update_ad(ad_id: int, ad_update: CompanyAdUpdate,
              current_user: UserData = Depends(get_current_active_professional)):

    def _update_ad():
        result = update(ad_id, ad_update, current_user.id)
        return result

    return process_request(_update_ad)
