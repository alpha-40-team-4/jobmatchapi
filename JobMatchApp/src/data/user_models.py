# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=import-error
# pylint: disable=too-few-public-methods
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=redefined-builtin

from pydantic import BaseModel
from src.data.info_models import email_type


class CompanyRegData(BaseModel):
    username: str
    password: str
    company_name: str
    email: email_type


class ProfessionalRegData(BaseModel):
    username: str
    password: str
    first_name: str
    last_name: str
    email: email_type


class UserData(BaseModel):
    id: int | None
    username: str
    hashed_password: str
    user_type_id: int
    approved: bool
    blocked: bool
    deleted: bool

    @classmethod
    def from_query_result(cls, id, username, password, user_type_id, approved, blocked, deleted):
        return cls(id=id,
                   username=username,
                   hashed_password=password,
                   user_type_id=user_type_id,
                   approved=approved,
                   blocked=blocked,
                   deleted=deleted)
