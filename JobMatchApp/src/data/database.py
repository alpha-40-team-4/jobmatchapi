# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring

import os
from mariadb import connect
from mariadb.connections import Connection
import cloudinary
import cloudinary.uploader
import cloudinary.api
from dotenv import load_dotenv

load_dotenv()

cloudinary.config(cloud_name=os.getenv('cloud_name'),
                  api_key=os.getenv('cloud_api_key'),
                  api_secret=os.getenv('cloud_api_secret'),
                  secure=True
                  )


def upload_image(image_url: str) -> str:
    """
    Receives a URL from the image uploaded to a public storage bucket.
    Uploads the resource to Cloudinary and returns the source URL string.
    :param image_url:
    :return: str
    """
    cloudinary.uploader.upload(file=image_url, unique_filename=False, overwrite=True)
    src_url = cloudinary.CloudinaryImage("portrait_pic").build_url()
    return src_url


def get_connection() -> Connection:
    return connect(
        user='Team4',
        password='Team4Jobs2022*',
        host='nlikyov.asuscomm.com',
        port=49495,
        database='team_4_job_match'
    )


def read_query(sql: str, sql_params=()) -> list:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)


def insert_query(sql: str, sql_params=()) -> int:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid


def update_query(sql: str, sql_params=()) -> bool:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount


def delete_query(sql: str, sql_params=()) -> bool:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount


def process_query_transactional(sql: str, conn: Connection, sql_params=()) -> int:
    cursor = conn.cursor()
    cursor.execute(sql, sql_params)

    return cursor.lastrowid


def update_query_transactional(sql: str, conn: Connection, sql_params=()) -> bool:
    cursor = conn.cursor()
    cursor.execute(sql, sql_params)

    return cursor.rowcount
