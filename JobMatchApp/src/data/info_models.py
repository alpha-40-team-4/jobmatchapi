# pylint: disable=anomalous-backslash-in-string
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=import-error
# pylint: disable=too-few-public-methods
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=redefined-builtin

from pydantic import BaseModel, constr
from src.data.app_models import CompanyAdMatchRequest


email_type = constr(regex='^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$')


class ProfInfoUpdate(BaseModel):
    summary: str | None
    active: bool | None
    photo: str | None
    location_id: int | None
    first_name: str | None
    last_name: str | None
    main_company_ad_id: int | None
    visible_matches: bool | None


class ProfInfo(BaseModel):
    id: int | None
    summary: str | None
    active: str | None
    photo: str | None
    location_id: int | None
    email: email_type
    first_name: str
    last_name: str
    main_company_ad_id: int | None
    visible_matches: bool | None

    @classmethod
    def from_query_result(cls, id, summary, active, photo, location_id, email, first_name,
                          last_name, main_company_ad_id, visible_matches):

        return cls(
            id=id,
            summary=summary,
            active=active,
            photo=photo,
            location_id=location_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            main_company_ad_id=main_company_ad_id,
            visible_matches=visible_matches)


class ProfInfoFull(ProfInfo):
    number_active_ads: int | None
    matches: list[CompanyAdMatchRequest] | None

    @classmethod
    def from_query_result(cls, id, summary, active, photo, location_id, email,
                          first_name, last_name, main_company_ad_id, visible_matches,
                          number_active_ads, matches):

        info = cls(
            id=id,
            summary=summary,
            active=active,
            photo=photo,
            location_id=location_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            main_company_ad_id=main_company_ad_id,
            visible_matches=visible_matches,
            number_active_ads=number_active_ads,
            matches=matches)

        if info.visible_matches is False:
            return info.copy(exclude={'visible_matches', 'matches'})

        return info.copy(exclude={'visible_matches'})


class CompanyInfoUpdate(BaseModel):
    name: str | None
    description: str | None
    contacts: str | None
    logo: str | None
    location_id: int | None


class CompanyInfo(BaseModel):
    id: int | None
    name: str
    description: str | None
    email: email_type
    contacts: str | None
    logo: str | None
    location_id: int | None

    @classmethod
    def from_query_result(cls, id, name, description, email, contacts, logo, location_id):
        return cls(
            id=id,
            name=name,
            description=description,
            email=email,
            contacts=contacts,
            logo=logo,
            location_id=location_id
        )


class CompanyInfoFull(CompanyInfo):
    number_active_ads: int | None
    number_matches: int | None

    @classmethod
    def from_query_result(cls, id, name, description, email, contacts, logo, location_id,
                          number_active_ads, number_matches):

        return cls(
            id=id,
            name=name,
            description=description,
            email=email,
            contacts=contacts,
            logo=logo,
            location_id=location_id,
            number_active_ads=number_active_ads,
            number_matches=number_matches
        )
