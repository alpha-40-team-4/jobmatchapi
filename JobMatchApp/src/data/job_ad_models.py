# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=import-error
# pylint: disable=no-self-argument
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=redefined-builtin
# pylint: disable=too-few-public-methods

from enum import Enum
from pydantic import BaseModel, Field, root_validator
from src.common.exceptions import BadRequestException
from src.data.app_models import Skill, MatchRequest


class JobAdStatus(str, Enum):
    ACTIVE = 'active'
    MATCHED = 'matched'


class JobAdStatusAll(str, Enum):
    ACTIVE = 'active'
    MATCHED = 'matched'
    DELETED = 'deleted'


class JobAdCreate(BaseModel):
    salary_min: int = Field(ge=0)
    salary_max: int
    description: str
    location_id: int | None
    remote: bool
    skills: list[Skill] = Field(min_items=1)

    @root_validator
    def validate_max_salary(cls, values):
        return _validate_max_salary(values)

    @root_validator
    def validate_location(cls, values):
        return _validate_location(values)


class JobAdRepr(BaseModel):
    id: int
    salary_min: int
    salary_max: int
    description: str
    location_id: int | None
    remote: bool
    status: JobAdStatus
    companies_users_id: int

    @classmethod
    def from_query(cls, id, salary_min, salary_max, description, location_id, remote, status,
                   companies_users_id):
        return cls(
            id=id,
            salary_min=salary_min,
            salary_max=salary_max,
            description=description,
            location_id=location_id,
            remote=remote,
            status=status,
            companies_users_id=companies_users_id)


class JobAd(BaseModel):
    id: int
    salary_min: int = Field(ge=0)
    salary_max: int
    description: str
    location_id: int | None
    remote: bool
    status: JobAdStatusAll
    companies_users_id: int
    skills: list[Skill]

    @root_validator
    def validate_max_salary(cls, values):
        return _validate_max_salary(values)

    @root_validator
    def validate_location(cls, values):
        return _validate_location(values)

    @classmethod
    def from_query(cls, id, salary_min, salary_max, description, location_id, remote, status,
                   companies_users_id, skills):
        s = skills
        if isinstance(skills, str):
            skills_list = skills.split(',')
            t = [el.split(':') for el in skills_list]
            s = [Skill.from_query(el[0], el[1]) for el in t]

        return cls(
            id=id,
            salary_min=salary_min,
            salary_max=salary_max,
            description=description,
            location_id=location_id,
            remote=remote,
            status=status,
            companies_users_id=companies_users_id,
            skills=s
            )


class JobAdWithMatches(BaseModel):
    id: int
    salary_min: int
    salary_max: int
    description: str
    location_id: int | None
    remote: bool
    status: JobAdStatus
    companies_users_id: int
    skills: list[Skill]
    matches: list[MatchRequest] | None


    @classmethod
    def from_query(cls, id, salary_min, salary_max, description, location_id,
                   remote, status, companies_users_id, skills, matches):
        return cls(
            id=id,
            salary_min=salary_min,
            salary_max=salary_max,
            description=description,
            location_id=location_id,
            remote=remote,
            status=status,
            companies_users_id=companies_users_id,
            skills=[Skill.from_query(el[0], el[1]) for el in skills],
            matches=[MatchRequest.from_query(el[0], el[1], el[2]) for el in matches])


class JobAdUpdate(BaseModel):
    salary_min: int | None = Field(ge=0)
    salary_max: int | None
    description: str | None
    location_id: int | None
    remote: bool | None
    skills: list[Skill] | None

    @classmethod
    def from_query(cls, salary_min, salary_max, description, location_id, remote,
                   skills):
        return cls(
            salary_min=salary_min,
            salary_max=salary_max,
            description=description,
            location_id=location_id,
            remote=remote,

            skills=[Skill.from_query(el[0], el[1]) for el in skills]
            )


def _validate_max_salary(values):
    salary_min, salary_max = values.get('salary_min'), values.get('salary_max')
    if salary_min is not None and salary_max is not None and salary_max < salary_min:
        raise BadRequestException('Max salary should be higher than min salary.')
    return values


def _validate_location(values):
    location_id, remote = values.get('location_id'), values.get('remote')
    if remote is False and not location_id:
        raise BadRequestException('Provide location_id if Job_ad should be remote.')
    return values
