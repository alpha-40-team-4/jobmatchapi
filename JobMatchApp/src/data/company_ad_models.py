# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=redefined-builtin
# pylint: disable=too-few-public-methods
# pylint: disable=import-error
# pylint: disable=no-self-argument
# pylint: disable=too-many-arguments

from enum import Enum
from pydantic import BaseModel, NonNegativeInt, root_validator
from src.data.app_models import MatchRequest, Skill


class CompAdsStatus(str, Enum):
    ACTIVE = 'active'
    HIDDEN = 'hidden'
    PRIVATE = 'private'


class CompAdsStatusAll(str, Enum):
    ACTIVE = 'active'
    HIDDEN = 'hidden'
    PRIVATE = 'private'
    MATCHED = 'matched'
    DELETED = 'deleted'


class CompanyAdCreate(BaseModel):
    salary_min: NonNegativeInt
    salary_max: NonNegativeInt
    description: str
    location_id: int | None
    remote: bool
    status: CompAdsStatus
    skills_levels: list[Skill]

    @root_validator
    def validate_max_salary(cls, values):
        salary_min, salary_max = values.get('salary_min'), values.get('salary_max')
        if salary_max < salary_min:
            raise ValueError('Max salary should be greater than min salary')
        return values

    @root_validator
    def validate_location(cls, values):
        location_id, remote = values.get('location_id'), values.get('remote')
        if remote is False and not location_id:
            raise ValueError('Provide location_id or set remote = True')
        return values


class CompanyAd(CompanyAdCreate):
    id: int
    user_id: int
    status: CompAdsStatusAll

    @classmethod
    def from_query(cls, id, salary_min, salary_max, description, remote, status,
                   location_id, user_id, skills_levels):
        return cls(
            id=id,
            salary_min=salary_min,
            salary_max=salary_max,
            description=description,
            remote=remote,
            status=status,
            location_id=location_id,
            user_id=user_id,
            skills_levels=skills_levels)


class CompanyAdWithRequests(CompanyAd):
    match_requests: list[MatchRequest]

    @classmethod
    def from_ad(cls, comp_ad: CompanyAd, match_requests: list[MatchRequest]):
        return cls(
            id=comp_ad.id,
            salary_min=comp_ad.salary_min,
            salary_max=comp_ad.salary_max,
            description=comp_ad.description,
            remote=comp_ad.remote,
            status=comp_ad.status,
            location_id=comp_ad.location_id,
            user_id=comp_ad.user_id,
            skills_levels=comp_ad.skills_levels,
            match_requests=match_requests)


class CompanyAdUpdate(BaseModel):
    salary_min: NonNegativeInt | None
    salary_max: NonNegativeInt | None
    description: str | None
    remote: bool | None
    status: CompAdsStatus | None
    location_id: int | None
    skills_levels: list[Skill] | None
