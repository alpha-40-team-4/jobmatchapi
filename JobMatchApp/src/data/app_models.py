# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=no-name-in-module
# pylint: disable=invalid-name
# pylint: disable=redefined-builtin
# pylint: disable=too-few-public-methods

from enum import Enum
from pydantic import BaseModel


class LocationInfo(BaseModel):
    id: int
    city: str

    @classmethod
    def from_query(cls, id, city):
        return cls(id=id, city=city)


class LocationStatus(str, Enum):
    ACTIVE = 'active'
    PENDING = 'pending'
    DELETED = 'deleted'


class SkillInfo(BaseModel):
    id: int
    name: str

    @classmethod
    def from_query(cls, id, name):
        return cls(id=id, name=name)


class SkillStatus(str, Enum):
    ACTIVE = 'active'
    PENDING = 'pending'
    DELETED = 'deleted'


class Level(BaseModel):
    id: int
    level: str

    @classmethod
    def from_query(cls, id, level):
        return cls(id=id, level=level)


class Skill(BaseModel):
    skill_id: int
    level_id: int

    @classmethod
    def from_query(cls, skill_id, level_id):
        return cls(skill_id=skill_id, level_id=level_id)


class MatchRequest(BaseModel):
    id: int
    matched: bool
    user_id: int

    @classmethod
    def from_query(cls, id, matched, user_id):
        return cls(id=id, matched=matched, user_id=user_id)


class CompanyAdMatchRequest(BaseModel):
    id: int
    matched: bool
    user_id: int
    company_ad_id: int

    @classmethod
    def from_query(cls, id, matched, user_id, company_ad_id):
        return cls(id=id, matched=matched, user_id=user_id, company_ad_id=company_ad_id)


class JobAdMatchRequest(BaseModel):
    id: int
    matched: bool
    user_id: int
    job_ad_id: int

    @classmethod
    def from_query(cls, id, matched, user_id, job_ad_id):
        return cls(id=id, matched=matched, user_id=user_id, job_ad_id=job_ad_id)
