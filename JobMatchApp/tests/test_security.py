# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=import-error

from datetime import timedelta, datetime, timezone
from unittest.mock import Mock

import pytest
from fastapi import HTTPException, status
from jose import jwt

from src.main import app
from src.common.security import authenticate_user, create_access_token, get_current_admin, \
    get_current_active_company_or_admin, get_current_active_company, get_current_active_professional_or_admin, \
    get_current_active_professional, get_current_user
from src.common.security import SECRET_KEY, ALGORITHM
from src.data.user_models import UserData


def generate_user_data(user_type_id: int = 1, approved: bool = True,
                       blocked: bool = False, deleted: bool = False):
    return UserData(id=1,
                    username='test',
                    hashed_password='test',
                    user_type_id=user_type_id,
                    approved=approved,
                    blocked=blocked,
                    deleted=deleted)


def override_dependency(user_type_id: int):
    return generate_user_data(user_type_id)


@pytest.mark.parametrize('user, ver_pass, expected', [(None, None, False),
                                                      (generate_user_data(), False, False),
                                                      (generate_user_data(), True,
                                                       generate_user_data())])
def test_authenticate_user(mocker, user, ver_pass, expected):
    mocker.patch('src.services.users_service.get_user_by_username', return_value=user)
    mocker.patch('src.common.security.verify_password', return_value=ver_pass)
    result = authenticate_user('username', 'password')
    assert result == expected


@pytest.mark.parametrize('expires_delta', [None, timedelta(minutes=10)])
def test_create_access_token(mocker, expires_delta):
    data = {"sub": 'test_username'}
    fake_now = datetime(2022, 11, 15, 20, 0, 0, tzinfo=timezone.utc)
    mock_date = mocker.patch("src.common.security.datetime")
    mock_date.utcnow.return_value = fake_now
    if expires_delta is None:
        expire = fake_now + timedelta(minutes=15)
    else:
        expire = fake_now + expires_delta
    to_encode = data.copy()
    to_encode.update({"exp": expire})
    expected = jwt.encode(to_encode,
                          SECRET_KEY, algorithm=ALGORITHM)
    result = create_access_token(data, expires_delta)
    assert result == expected


def test_get_current_user(mocker):
    decode_mock = mocker.patch('src.common.security.jwt')
    decode_mock.decode.return_value = {'sub': 'username'}
    mocker.patch('src.services.users_service.get_user_by_username',
                 return_value=generate_user_data())
    result = get_current_user('fake_token')
    assert result == generate_user_data()


@pytest.mark.parametrize('username, user', [(None, None),
                                            ('username', None),
                                            ('username', generate_user_data(approved=False)),
                                            ('username', generate_user_data(blocked=True)),
                                            ('username', generate_user_data(deleted=True))])
def test_get_current_user_raises_exception(mocker, username, user):
    decode_mock = mocker.patch('src.common.security.jwt')
    decode_mock.decode.return_value = {'sub': username}
    mocker.patch('src.services.users_service.get_user_by_username',
                 return_value=user)
    with pytest.raises(HTTPException) as ex:
        result = get_current_user('fake_token')
        assert result is None
        assert ex.type == HTTPException


def test_get_current_active_professional():
    expected = generate_user_data(1)
    result = get_current_active_professional(generate_user_data(user_type_id=1))
    assert result == expected


@pytest.mark.parametrize('user_type_id', [2, 3])
def test_get_current_active_professional_raises_exception(user_type_id):
    with pytest.raises(HTTPException) as ex:
        result = get_current_active_professional(generate_user_data(user_type_id=user_type_id))
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == "User should be registered as professional"


@pytest.mark.parametrize('user_type_id', [1, 3])
def test_get_current_active_professional_or_admin(user_type_id):
    expected = generate_user_data(user_type_id)
    result = get_current_active_professional_or_admin(generate_user_data(user_type_id=user_type_id))
    assert result == expected


def test_get_current_active_professional_or_admin_raises_exception():
    with pytest.raises(HTTPException) as ex:
        result = get_current_active_professional_or_admin(generate_user_data(user_type_id=2))
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == "User should be registered as professional or admin"


def test_get_current_active_company():
    expected = generate_user_data(2)
    result = get_current_active_company(generate_user_data(user_type_id=2))
    assert result == expected


@pytest.mark.parametrize('user_type_id', [1, 3])
def test_get_current_active_company_raises_exception(user_type_id):
    with pytest.raises(HTTPException) as ex:
        result = get_current_active_company(generate_user_data(user_type_id=user_type_id))
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == "User should be registered as company"


@pytest.mark.parametrize('user_type_id', [2, 3])
def test_get_current_active_company_or_admin(user_type_id):
    expected = generate_user_data(user_type_id)
    result = get_current_active_company_or_admin(generate_user_data(user_type_id=user_type_id))
    assert result == expected


def test_get_current_active_company_or_admin_raises_exception():
    with pytest.raises(HTTPException) as ex:
        result = get_current_active_company_or_admin(generate_user_data(user_type_id=1))
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == "User should be registered as company or admin"


def test_get_current_admin():
    expected = generate_user_data(3)
    result = get_current_admin(generate_user_data(user_type_id=3))
    assert result == expected


@pytest.mark.parametrize('user_type_id', [1, 2])
def test_get_current_admin_raises_exception(user_type_id):
    with pytest.raises(HTTPException) as ex:
        result = get_current_admin(generate_user_data(user_type_id=user_type_id))
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == "User should be admin"
