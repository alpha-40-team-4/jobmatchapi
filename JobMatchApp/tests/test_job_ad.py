import pytest
from fastapi import HTTPException
from mariadb import Error

from src.data.user_models import UserData
from src.common.exceptions import BadRequestException, ForbiddenException, NotFoundException
from src.data.app_models import Skill
from src.data.job_ad_models import JobAdCreate, JobAdUpdate, JobAd, JobAdWithMatches, JobAdStatus
from src.services.job_ad_service import create, job_ad_update, get_job_ad_by_id_with_s_m, \
    get_ads_by_user_id, \
    _get_skills_by_ad_id, _get_matches_by_ad_id, _get_active_job_ad_by_id_with_s, \
    check_job_ad_eligibility, get_job_ads, get_ads_by_user


@pytest.fixture
def my_job_ad():
    return JobAd(id=1,
                 salary_min=1001,
                 salary_max=2001,
                 description='description',
                 location_id=None,
                 remote=True,
                 status='active',
                 skills=[{'skill_id': 8, 'level_id': 8}],
                 companies_users_id=1)


@pytest.fixture
def my_job_ad_with_1_match():
    return JobAdWithMatches(id=1,
                            salary_min=1005,
                            salary_max=2005,
                            description='description',
                            location_id=None,
                            remote=True,
                            status='active',
                            companies_users_id=1,
                            skills=[{'skill_id': 8, 'level_id': 8}, {'skill_id': 4, 'level_id': 3}],
                            matches=[{'id': 1, 'matched': True, 'user_id': 1}]
                            )


@pytest.fixture
def my_job_ad_with_1_match_when_prof():
    return JobAdWithMatches(id=1,
                            salary_min=1005,
                            salary_max=2005,
                            description='description',
                            location_id=None,
                            remote=True,
                            status='active',
                            companies_users_id=1,
                            skills=[{'skill_id': 8, 'level_id': 8}, {'skill_id': 4, 'level_id': 3}],
                            matches=[]
                            )


@pytest.fixture
def job_ad_repr_data():
    return [(1, 1005, 2005, 'description', None, True, 'active', 1)]


@pytest.fixture
def user_data():
    return UserData(
        id=1,
        username='Company_1',
        hashed_password='abc',
        user_type_id=2,
        approved=True,
        blocked=False,
        deleted=False)


@pytest.fixture
def admin_user_data():
    return UserData(
        id=2,
        username='admin',
        hashed_password='abc',
        user_type_id=3,
        approved=True,
        blocked=False,
        deleted=False)


@pytest.fixture
def user_data_p():
    return UserData(
        id=3,
        username='prof',
        hashed_password='abc',
        user_type_id=1,
        approved=True,
        blocked=False,
        deleted=False)


@pytest.fixture
def my_job_ad_with_0_matches():
    return JobAdWithMatches(id=1,
                            salary_min=1005,
                            salary_max=2005,
                            description='description',
                            location_id=None,
                            remote=True,
                            status='active',
                            companies_users_id=1,
                            skills=[{'skill_id': 8, 'level_id': 8}],
                            matches=[]
                            )


@pytest.fixture
def my_updated_job_ad():
    return JobAd(id=1,
                 salary_min=1005,
                 salary_max=2005,
                 description='description',
                 location_id=None,
                 remote=True,
                 status='active',
                 skills=[{'skill_id': 8, 'level_id': 8}],
                 companies_users_id=1)


@pytest.fixture
def my_matched_job_ad():
    return JobAd(id=1,
                 salary_min=1005,
                 salary_max=2005,
                 description='description',
                 location_id=None,
                 remote=True,
                 status='matched',
                 skills=[{'skill_id': 8, 'level_id': 8}],
                 companies_users_id=1)


@pytest.fixture
def my_deleted_job_ad():
    return JobAd(id=1,
                 salary_min=1005,
                 salary_max=2005,
                 description='description',
                 location_id=None,
                 remote=True,
                 status='deleted',
                 skills=[{'skill_id': 8, 'level_id': 8}],
                 companies_users_id=1)


@pytest.fixture
def my_job_ad_update_for_salaries():
    return JobAdUpdate(id=1,
                       salary_min=1005,
                       salary_max=2005)


@pytest.fixture
def my_job_ad_update_for_invalid_skills():
    s = ((1, 8), (1, 8))
    skills = (Skill(skill_id=skill_id,
                    level_id=level_id) for skill_id, level_id in s)
    return JobAdUpdate(skills=skills)


def generate_valid_job_ad():
    return JobAdCreate(salary_min=5, salary_max=5000,
                       description='description', location_id=1, remote=True,
                       skills=[Skill(skill_id=1, level_id=1), Skill(skill_id=2, level_id=2)])


def generate_job_ad_invalid_skills():
    return JobAdCreate(salary_min=1, salary_max=2,
                       description='description', location_id=None, remote=False,
                       skills=[Skill(skill_id=1, level_id=1)])


def generate_job_ad_invalid_min_max_salary():
    return JobAdCreate(salary_min=5, salary_max=2,
                       description='description', location_id=1, remote=True,
                       skills=[Skill(skill_id=1, level_id=1)])


def generate_almost_valid_job_ad():
    skill = Skill(skill_id=1, level_id=8)
    return JobAdCreate(salary_min=5, salary_max=5000,
                       description='description', location_id=1, remote=True,
                       skills=[Skill(skill_id=1, level_id=8)])


def generate_job_ad_invalid_location_id_remote_status():
    return JobAdCreate(salary_min=1, salary_max=2,
                       description='description', location_id=None, remote=False,
                       skills=[Skill(skill_id=1, level_id=1)])


@pytest.fixture
def j_ad():
    return JobAd(id=1,
                 salary_min=5,
                 salary_max=5000,
                 description='description',
                 location_id=1,
                 remote=True,
                 status='active',
                 skills=[{'skill_id': 1, 'level_id': 1}, {'skill_id': 2, 'level_id': 2}],
                 companies_users_id=1)


def test_create_job_ad_with_valid_input_(mocker, j_ad):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', return_value=1)
    mocker.patch('src.services.job_ad_service.get_job_ad_creator', return_value=[['Company_1']])

    mocker.patch('src.services.job_ad_service.create_tweet', return_value={})
    job_ad = create(generate_valid_job_ad(), 1)

    assert job_ad is not None


def test_create_job_ad_with_invalid_input_ad_raises_error_when_invalid_salary_levels(mocker):
    mocker.patch('src.data.database.process_query_transactional', return_value=0)

    with pytest.raises(BadRequestException) as ex:
        ad = create(generate_job_ad_invalid_min_max_salary(), 1)
        assert ad is None
        assert ex.type == BadRequestException


def test_create_job_ad_with_valid_input_but_skill_is_deleted(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "rollback.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
    mocker.patch('src.data.database.read_query', return_value=[['Company_1']])
    mocker.patch('src.services.job_ad_service.create_tweet', return_value={})

    with pytest.raises(Error) as ex:
        job_ad = create(generate_almost_valid_job_ad(), 1)
        assert job_ad is None
        assert ex.type == Error
        mock_conn.rollback().assert_called_once()


def test_create_job_ad_with_invalid_input_ad_raises_error_when_invalid_location_id_remote(mocker):
    mocker.patch('src.data.database.process_query_transactional', return_value=0)

    with pytest.raises(BadRequestException) as ex:
        ad = create(generate_job_ad_invalid_location_id_remote_status(), 1)
        assert ad is None
        assert ex.type == BadRequestException


def test_create_job_ad_with_invalid_input_for_raises_error_when_invalid_skills(mocker):
    mocker.patch('src.data.database.process_query_transactional', return_value=1)

    with pytest.raises(BadRequestException):
        ad = create(generate_job_ad_invalid_skills(), 1)
        assert ad is None


job_ads_to_update = [
    JobAdUpdate(salary_min=200),
    JobAdUpdate(salary_max=2000),
    JobAdUpdate(description='new d'),
    JobAdUpdate(remote=False),
    JobAdUpdate(location_id=2),
    JobAdUpdate(skills=[Skill(skill_id=7, level_id=8)]),
    JobAdUpdate(skills=[Skill(skill_id=4, level_id=8)]),
    JobAdUpdate(skills=[Skill(skill_id=7, level_id=8), Skill(skill_id=4, level_id=8)]),
    JobAdUpdate(remote=False, skills_levels=[Skill(skill_id=7, level_id=8)])
]


@pytest.mark.parametrize('jobad_update', job_ads_to_update)
def test_update_job_add_with_valid_data(
        mocker, my_job_ad, my_updated_job_ad, jobad_update):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_job_ad)
    mocker.patch('src.services.job_ad_service.update_query', return_value=1)
    mocker.patch('src.data.database.process_query_transactional', return_value=1)

    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_updated_job_ad)
    if jobad_update.skills:
        skills_ids = set(
            [s.skill_id for s in my_job_ad.skills]
            + [s.skill_id for s in jobad_update.skills]
        )
        new = []
        for skill_id in skills_ids:
            current = next((s for s in my_job_ad.skills if s.skill_id == skill_id), None)
            updated = next((s for s in jobad_update.skills if s.skill_id == skill_id), None)
            new.append(updated if updated else current)
    else:
        new = my_job_ad.skills
    result = job_ad_update(1, jobad_update, 1)
    assert result == my_updated_job_ad
    assert result.salary_min == my_updated_job_ad.salary_min
    assert result.salary_max == my_updated_job_ad.salary_max
    assert result.description == my_updated_job_ad.description
    assert result.remote == my_updated_job_ad.remote
    assert result.location_id == my_updated_job_ad.location_id
    assert result.skills == my_updated_job_ad.skills


def test_update_job_add_with_invalid_data_for_owner(
        mocker, my_job_ad, my_job_ad_update_for_salaries):
    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_job_ad)
    mocker.patch('src.services.job_ad_service.update_query', return_value=1)
    with pytest.raises(ForbiddenException):
        result = job_ad_update(1, my_job_ad_update_for_salaries, 2)
        assert result == ForbiddenException(
            content='You are not the author of this ad and cannot update it.')


def test_update_job_add_with_invalid_status_matched(
        mocker, my_matched_job_ad, my_job_ad_update_for_salaries):
    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_matched_job_ad)
    mocker.patch('src.services.job_ad_service.update_query', return_value=1)

    with pytest.raises(BadRequestException):
        result = job_ad_update(1, my_job_ad_update_for_salaries, 1)
        assert result == BadRequestException(
            content='Ad has already been matched and cannot be edited.')


def test_update_job_add_with_invalid_status_deleted(
        mocker, my_deleted_job_ad, my_job_ad_update_for_salaries):
    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_deleted_job_ad)
    mocker.patch('src.services.job_ad_service.update_query', return_value=1)

    with pytest.raises(NotFoundException):
        result = job_ad_update(1, my_job_ad_update_for_salaries, 1)
        assert result == NotFoundException(content='Ad has been deleted.')


def test_update_job_add_with_invalid_skills(
        mocker, my_job_ad, my_job_ad_update_for_invalid_skills):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "rollback.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.update_query', return_value=1)
    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
    mocker.patch('src.services.job_ad_service._get_active_job_ad_by_id_with_s',
                 return_value=my_job_ad)

    with pytest.raises(Error):
        result = job_ad_update(1, my_job_ad_update_for_invalid_skills, 1)
        assert result == Error


def test_get_job_by_id_with_valid_data_0_matches(
        mocker, job_ad_repr_data, user_data, my_job_ad_with_0_matches):
    mocker.patch('src.data.database.read_query', return_value=job_ad_repr_data)
    mocker.patch('src.services.job_ad_service._get_skills_by_ad_id', return_value=[(8, 8)])
    mocker.patch('src.services.job_ad_service._get_matches_by_ad_id', return_value=[])
    expected = my_job_ad_with_0_matches
    result = get_job_ad_by_id_with_s_m(1, user_data)
    assert result == expected


def test_get_job_by_id_where_no_valid_data(mocker, user_data):
    mocker.patch('src.data.database.read_query', return_value=[])
    with pytest.raises(NotFoundException) as ex:
        ad = get_job_ad_by_id_with_s_m(3, user_data)
        assert ex.type == NotFoundException(content='Ad not found')


skills = [
    (8, 8), (4, 3)
]


@pytest.mark.parametrize('skills_t', skills)
def test_get_job_by_id_with_valid_data_with_match(
        mocker, job_ad_repr_data, user_data, my_job_ad_with_1_match,
                                                  skills_t):
    mocker.patch('src.data.database.read_query', return_value=job_ad_repr_data)
    mocker.patch('src.services.job_ad_service._get_skills_by_ad_id', return_value=skills)
    mocker.patch('src.services.job_ad_service._get_matches_by_ad_id', return_value=[(1, True, 1)])
    expected = my_job_ad_with_1_match
    result = get_job_ad_by_id_with_s_m(1, user_data)
    assert result == expected


@pytest.mark.parametrize('skills_t', skills)
def test_get_job_by_id_with_valid_data_with_match(
        mocker, job_ad_repr_data, admin_user_data, my_job_ad_with_1_match,
                                                  skills_t):
    mocker.patch('src.data.database.read_query', return_value=job_ad_repr_data)
    mocker.patch('src.services.job_ad_service._get_skills_by_ad_id', return_value=skills)
    mocker.patch('src.services.job_ad_service._get_matches_by_ad_id', return_value=[(1, True, 1)])
    expected = my_job_ad_with_1_match
    result = get_job_ad_by_id_with_s_m(1, admin_user_data)
    assert result == expected


@pytest.mark.parametrize('skills_t', skills)
def test_get_job_by_id_with_valid_data_with_match_when_prof(
        mocker, job_ad_repr_data, user_data_p, my_job_ad_with_1_match_when_prof,
        skills_t):
    mocker.patch('src.data.database.read_query', return_value=job_ad_repr_data)
    mocker.patch('src.services.job_ad_service._get_skills_by_ad_id', return_value=skills)
    mocker.patch('src.services.job_ad_service._get_matches_by_ad_id', return_value=[(1, True, 1)])
    expected = my_job_ad_with_1_match_when_prof
    result = get_job_ad_by_id_with_s_m(1, user_data_p)
    assert result == expected


@pytest.fixture
def own_job_ads_all():
    return [(1, 0, 200, 'description', None, True, 'active', 1, '8:8'),
            (1, 101, 101, 'description1', None, True, 'active', 1, '4:8'),
            (1, 1, 1000, 'description1', None, True, 'matched', 1, '7:8')
            ]


@pytest.fixture
def own_job_ads_active():
    return [(1, 0, 200, 'description', None, True, 'active', 1, '8:8'),
            (1, 101, 101, 'description1', None, True, 'active', 1, '4:8'),
            ]


def test_get_ads_by_user_id_all(mocker, user_data, own_job_ads_all):
    mocker.patch('src.data.database.read_query', return_value=own_job_ads_all)
    result = get_ads_by_user_id(user_data.id)

    assert result is not None
    assert isinstance(result, list)


def test_get_ads_by_user_id_active(mocker, user_data, own_job_ads_active):
    mocker.patch('src.data.database.read_query', return_value=own_job_ads_active)
    result = get_ads_by_user_id(user_data.id, JobAdStatus.ACTIVE)

    assert result is not None
    assert isinstance(result, list)


@pytest.fixture
def my_skill_set():
    return [(7, 8), (4, 8)]


def test_skills_by_ad(mocker, my_skill_set):
    mocker.patch('src.data.database.read_query', return_value=my_skill_set)
    result = _get_skills_by_ad_id(1)
    assert result is not None
    assert isinstance(result, list)


@pytest.fixture
def my_match_requests():
    return [(1, False, 1), (2, False, 1)]


def test_get_matches_by_ad_id(mocker, my_match_requests):
    mocker.patch('src.data.database.read_query', return_value=my_match_requests)
    result = _get_matches_by_ad_id(1)
    assert result is not None
    assert isinstance(result, list)


@pytest.fixture
def job_ads():
    return [(1, 0, 200, 'description', None, True, 'active', 1, '8:8')]


ad = JobAd.from_query(id=1,
                      salary_min=0,
                      salary_max=200,
                      description='description',
                      location_id=None,
                      remote=True,
                      status='active',
                      companies_users_id=1,
                      skills='8:8')


def test_get_active_job_ad_by_id_with_s(mocker, job_ads):
    mocker.patch('src.data.database.read_query', return_value=job_ads)
    result = _get_active_job_ad_by_id_with_s(1)
    assert result == ad


def test_get_active_job_ad_by_id_with_s_where_no_ad(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    with pytest.raises(NotFoundException):
        result = _get_active_job_ad_by_id_with_s(22)
        assert result == NotFoundException


def test_check_job_ad_eligibility_when_exists(mocker):
    mocker.patch('src.data.database.read_query', return_value=job_ads)
    with pytest.raises(HTTPException):
        result = check_job_ad_eligibility(1)
        assert result == HTTPException(status_code=403)


def test_check_job_ad_eligibility_when_does_not_exist(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])

    result = check_job_ad_eligibility(1)
    assert result is None


params = [
    {'salary_min': 100, 'salary_max': None, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': 200, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': 100, 'salary_max': 200, 'salary_threshold': 0.10,
     'location_id': None, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': None, 'remote': True, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': False, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': ['8,8'],
     'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': True, 'skills': ['1,1', '8,8'],
     'skill_threshold': 1},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': None, 'remote': True, 'skills': ['8,7', '2,2'],
     'skill_threshold': 1}
]

r1 = (1, 1005, 2005, 'description', None, True, 'active', 1, '8:8')
r2 = (2, 1005, 2005, 'description', None, True, 'active', 1, '8:8')

job_ad = JobAd(id=1,
               salary_min=1005,
               salary_max=2005,
               description='description',
               location_id=None,
               remote=True,
               status='active',
               companies_users_id=1,
               skills=[{'skill_id': 8, 'level_id': 8}]
               )

job_ad_1 = job_ad
job_ad_2 = job_ad.copy(update={'id': 2})
expected = [job_ad_1, job_ad_2]


@pytest.mark.parametrize('param', params)
def test_get_ads(mocker, param):
    mocker.patch('src.data.database.read_query', return_value=(r1, r2))

    result = get_job_ads(**param)
    assert result is not None
    assert expected == result
    assert isinstance(result, list)


params_returns_empty = [
    {'salary_min': 100, 'salary_max': 200, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': 100, 'salary_max': 5000, 'salary_threshold': 0,
     'location_id': 15, 'remote': None, 'skills': [], 'skill_threshold': 0},
    {'salary_min': None, 'salary_max': None, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': ['15,15'],
     'skill_threshold': 0},

    ]


@pytest.mark.parametrize('par', params_returns_empty)
def test_get_ads_returns_empty_list(mocker, par):
    mocker.patch('src.data.database.read_query', return_value=[])

    result = get_job_ads(**par)
    assert result is not None
    assert isinstance(result, list)


def test_get_ads_by_user_raises_403_when_no_user(mocker):
    mocker.patch('src.services.job_ad_service.get_user_by_id', return_value=None)
    with pytest.raises(HTTPException) as ex:
        result = get_ads_by_user(5)
        assert result == HTTPException(status_code=403)


@pytest.fixture
def user_data_deleted():
    return UserData(
        id=2,
        username='prof',
        hashed_password='abc',
        user_type_id=2,
        approved=True,
        blocked=False,
        deleted=True)


def test_get_ads_by_user_raises_403_when_user_deleted(mocker, user_data_deleted):
    mocker.patch('src.services.job_ad_service.get_user_by_id', return_value=user_data_deleted)
    with pytest.raises(HTTPException) as ex:
        result = get_ads_by_user(5)
        assert result == HTTPException(status_code=403)


def test_get_ads_by_user_raises_400(mocker, user_data_p):
    mocker.patch('src.services.job_ad_service.get_user_by_id', return_value=user_data_p)
    with pytest.raises(HTTPException):
        result = get_ads_by_user(3)
        assert result == HTTPException(status_code=400)


def test_get_ads_by_user_returns_ads(mocker, user_data):
    mocker.patch('src.services.job_ad_service.get_user_by_id', return_value=user_data)
    mocker.patch('src.services.job_ad_service.get_ads_by_user_id', return_value=[])
    result = get_ads_by_user(user_data.id)
    assert result == []
