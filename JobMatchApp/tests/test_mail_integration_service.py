from src.common.exceptions import NotFoundException
from src.services.mail_integration_service import _get_recipient_company_email_and_name, \
    _get_recipient_professional_email_and_name


def test_get_recipient_company_email_and_name_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[('company1@gmail.com', 'company1')])
    result = _get_recipient_company_email_and_name(1)
    assert result == [('company1@gmail.com', 'company1')]


def test_get_recipient_company_email_and_name_when_unsuccessful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = _get_recipient_company_email_and_name(1)
    assert isinstance(result, NotFoundException)
    assert result.args[0] == 'Author of job ad with id: 1 not found'


def test_get_recipient_professional_email_and_name_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[('prof1@gmail.com', 'prof1')])
    result = _get_recipient_professional_email_and_name(1)
    assert result == [('prof1@gmail.com', 'prof1')]


def test_get_recipient_professional_email_and_name_when_unsuccessful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = _get_recipient_professional_email_and_name(1)
    assert isinstance(result, NotFoundException)
    assert result.args[0] == 'Author of company ad with id: 1 not found'
