import pytest
from fastapi import HTTPException
from src.data.app_models import JobAdMatchRequest, CompanyAdMatchRequest
from src.data.user_models import ProfessionalRegData, CompanyRegData, UserData
from src.data.info_models import CompanyInfo, ProfInfo
from src.services.users_service import register_professional_wrapper, register_company_wrapper, get_user_by_username, \
    get_user_by_id, get_pending_registrations, confirm_user_registration, get_blocked_users, update_blocked_unblocked, \
    delete_users, delete_comp_ad, del_job_ad, get_sent_requests_by_prof_id, get_sent_requests_by_company_id, \
    get_company_by_id, get_professional_by_id, register_company, register_professional


def generate_prof_user_data():
    return UserData(id=1,
                    username='prof',
                    hashed_password='asd',
                    user_type_id=1,
                    approved=True,
                    blocked=False,
                    deleted=False)


def generate_comp_user_data():
    return UserData(id=2,
                    username='comp',
                    hashed_password='asd',
                    user_type_id=2,
                    approved=True,
                    blocked=False,
                    deleted=False)


def generate_professional_reg_data():
    return ProfessionalRegData(username='prof',
                               password='asd',
                               first_name='test',
                               last_name='test1',
                               email='test@mail.com')


def generate_company_reg_data():
    return CompanyRegData(username='comp',
                          password='asd',
                          company_name='CompInc',
                          email='compinc@mail.com')


def generate_prof_info():
    return ProfInfo(id=1,
                    first_name='test',
                    last_name='test1',
                    email='test@gmail.com')


def generate_comp_info():
    return CompanyInfo(id=2,
                       name='CompInc',
                       description=None,
                       email='compinc@mail.com',
                       contacts=None,
                       logo=None,
                       location_id=1)


def test_register_professional_wrapper_when_user_already_exists(mocker):
    mocker.patch('src.services.users_service.get_user_by_username', return_value=generate_prof_user_data())
    with pytest.raises(HTTPException) as ex:
        register_professional_wrapper(generate_professional_reg_data())
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 422
    assert ex.value.detail == f'Username prof already exists'


def test_register_professional_wrapper_when_successful(mocker):
    mocker.patch('src.services.users_service.get_user_by_username', return_value=None)
    mocker.patch('src.services.users_service.register_professional', return_value=generate_prof_info())
    result = register_professional_wrapper(generate_professional_reg_data())
    assert result == generate_prof_info()


def test_register_company_wrapper_when_user_already_exists(mocker):
    mocker.patch('src.services.users_service.get_user_by_username', return_value=generate_comp_user_data())
    with pytest.raises(HTTPException) as ex:
        register_company_wrapper(generate_company_reg_data())
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 422
    assert ex.value.detail == f'Username comp already exists'


def test_register_company_wrapper_when_successful(mocker):
    mocker.patch('src.services.users_service.get_user_by_username', return_value=None)
    mocker.patch('src.services.users_service.register_company', return_value=generate_comp_info())
    result = register_company_wrapper(generate_company_reg_data())
    assert result == generate_comp_info()


def test_get_user_by_username_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(2, 'comp', 'asd', 2, True, False, False)])
    result = get_user_by_username('comp')
    assert result == generate_comp_user_data()


def test_get_user_by_username_when_user_not_found(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = get_user_by_username('comp')
    assert result is None


def test_get_user_by_id_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(2, 'comp', 'asd', 2, True, False, False)])
    result = get_user_by_id(2)
    assert result == generate_comp_user_data()


def test_get_user_by_id_when_user_not_found(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = get_user_by_id(2)
    assert result is None


@pytest.mark.parametrize('output, data', [([], []),
                                          ([(2, 'comp', 'asd', 2, True, False, False)],
                                           [generate_comp_user_data().copy(exclude={'hashed_password', 'deleted'})])])
def test_get_pending_registrations(mocker, output, data):
    mocker.patch('src.data.database.read_query', return_value=output)
    result = get_pending_registrations()
    assert result == data


def test_confirm_user_registration_when_successful(mocker):
    mocker.patch('src.data.database.update_query', return_value=1)
    result = confirm_user_registration([1])
    assert result == 'Registrations approved'


def test_confirm_user_registration_when_unsuccessful(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.update_query', return_value=0)
        confirm_user_registration([1])
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == 'Registrations already confirmed, users do not exist or were deleted'


@pytest.mark.parametrize('output, data', [([], []),
                                          ([(2, 'comp', 'asd', 2, True, True, False)],
                                           [UserData(id=2,
                                                     username='comp',
                                                     hashed_password='asd',
                                                     user_type_id=2,
                                                     approved=True,
                                                     blocked=True,
                                                     deleted=False).copy(exclude={'hashed_password', 'deleted'})])])
def test_get_blocked_users(mocker, output, data):
    mocker.patch('src.data.database.read_query', return_value=output)
    result = get_blocked_users()
    assert result == data


def test_update_blocked_unblocked_when_successful(mocker):
    mocker.patch('src.data.database.update_query', return_value=2)
    result = update_blocked_unblocked(True, [1, 2])
    assert result == 'Blocked status set successfully to True for users [1, 2]'


def test_update_blocked_unblocked_when_unsuccessful(mocker):
    mocker.patch('src.data.database.update_query', return_value=0)
    with pytest.raises(HTTPException) as ex:
        update_blocked_unblocked(True, [1, 2])
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == f'Users are already blocked/unblocked, do not exist or were deleted'


def test_delete_users_when_successful(mocker):
    mocker.patch('src.data.database.update_query', return_value=2)
    result = delete_users([1, 2])
    assert result == 'Users [1, 2] deleted'


def test_delete_users_when_unsuccessful(mocker):
    mocker.patch('src.data.database.update_query', return_value=0)
    with pytest.raises(HTTPException) as ex:
        delete_users([1, 2])
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == f'Users are already deleted or do not exist'


def test_delete_comp_ad_when_successful(mocker):
    mocker.patch('src.data.database.update_query', return_value=1)
    result = delete_comp_ad(1)
    assert result == 'Ad with id: 1 deleted'


def test_delete_comp_ad_when_unsuccessful(mocker):
    mocker.patch('src.data.database.update_query', return_value=0)
    with pytest.raises(HTTPException) as ex:
        delete_comp_ad(1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == f'Can not delete ad. It is matched, deleted or does not exist'


def test_del_job_ad_when_successful(mocker):
    mocker.patch('src.data.database.update_query', return_value=1)
    result = del_job_ad(1)
    assert result == 'Ad with id: 1 deleted'


def test_del_job_ad_when_unsuccessful(mocker):
    mocker.patch('src.data.database.update_query', return_value=0)
    with pytest.raises(HTTPException) as ex:
        del_job_ad(1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == f'Can not delete ad. It is matched, deleted or does not exist'


def test_get_sent_request_by_prof_id_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, False, 2, 3)])
    result = get_sent_requests_by_prof_id(2)
    assert result == [JobAdMatchRequest(id=1, matched=False, user_id=2, job_ad_id=3)]


def test_get_sent_request_by_company_id_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, False, 2, 3)])
    result = get_sent_requests_by_company_id(2)
    assert result == [CompanyAdMatchRequest(id=1, matched=False, user_id=2, company_ad_id=3)]


@pytest.mark.parametrize('output, data', [([], None), ([(2, 'CompInc', None, 'compinc@mail.com', None, None, 1)],
                                                       generate_comp_info())])
def test_get_company_by_id(mocker, output, data):
    mocker.patch('src.data.database.read_query', return_value=output)
    result = get_company_by_id(2)
    assert result == data


@pytest.mark.parametrize('output, data', [([], None), ([(1, None, None, None, None, 'test@gmail.com', 'test', 'test1',
                                                         None, None)],
                                                       ProfInfo(id=1, summary=None, active=None, photo=None,
                                                                location_id=None, email='test@gmail.com',
                                                                first_name='test', last_name='test1',
                                                                main_company_ad_id=None, visible_matches=None))])
def test_get_professional_by_id(mocker, output, data):
    mocker.patch('src.data.database.read_query', return_value=output)
    result = get_professional_by_id(1)
    assert result == data


def test_register_company_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.common.security.encode_password', return_value='encoded_password')
    mocker.patch('src.data.database.process_query_transactional', return_value=2)
    result = register_company(generate_company_reg_data())
    assert result == CompanyInfo(id=2, name='CompInc', description=None, email='compinc@mail.com', contacts=None,
                                 logo=None, location_id=None)


def test_register_company_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "rollback.return_value": 1
        }
    )
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.common.security.encode_password', return_value='encoded_password')
        mocker.patch('src.data.database.process_query_transactional', return_value=Exception)
        register_company(generate_company_reg_data())
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400


def test_register_professional_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.common.security.encode_password', return_value='encoded_password')
    mocker.patch('src.data.database.process_query_transactional', return_value=1)
    result = register_professional(generate_professional_reg_data())
    assert result == ProfInfo(id=1, summary=None, active=None, photo=None, location_id=None,
                              email='test@mail.com', first_name='test', last_name='test1', main_company_ad_id=None,
                              visiblle_matches=None)


def test_register_professional_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "rollback.return_value": 1
        }
    )
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.common.security.encode_password', return_value='encoded_password')
        mocker.patch('src.data.database.process_query_transactional', return_value=Exception)
        register_professional(generate_professional_reg_data())
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400

