import pytest
from mariadb import Error
from unittest.mock import call
from fastapi import HTTPException
from src.data.app_models import CompanyAdMatchRequest, JobAdMatchRequest
from src.services.match_request_service import create_company_ad_request, create_job_ad_request, \
    create_company_ad_request_wrapper, \
    create_job_ad_request_wrapper, check_if_company_ad_request_already_exists, \
    check_if_job_ad_request_already_exists, \
    match_with_professional_wrapper, match_with_professional, \
    match_with_company_wrapper, \
    check_if_match_request_status_is_false, get_job_match_request_by_id, \
    check_if_company_is_job_ad_author, get_company_match_request_by_id, \
    check_if_professional_is_company_ad_author, \
    get_match_requests, match_with_company


def test_create_company_ad_request(mocker):
    mocker.patch('src.data.database.insert_query', return_value=1)
    mail_prof = mocker.patch \
        ('src.services.mail_integration_service.send_match_email_to_professional')
    result = create_company_ad_request(1, 1)
    assert result == CompanyAdMatchRequest(id=1, matched=False, user_id=1, company_ad_id=1)
    calls = [call(1)]
    mail_prof.assert_has_calls([call(1)])


def test_create_job_ad_request(mocker):
    mocker.patch('src.data.database.insert_query', return_value=2)
    mail_comp = mocker.patch('src.services.mail_integration_service.send_match_email_to_company')
    result = create_job_ad_request(1, 2)
    assert result == JobAdMatchRequest(id=2, matched=False, user_id=1, job_ad_id=2)
    calls = [call(2)]
    mail_comp.assert_has_calls([call(2)])


def test_create_company_ad_request_wrapper(mocker):
    mocker.patch('src.services.match_request_service.check_if_company_ad_request_already_exists',
                 return_value=None)
    mocker.patch('src.services.company_ad_service.check_company_ad_eligibility',
                 return_value=None)
    mocker.patch('src.services.match_request_service.create_company_ad_request',
                 return_value=CompanyAdMatchRequest(id=1, matched=False, user_id=1,
                                                    company_ad_id=1))
    result = create_company_ad_request_wrapper(1, 1)
    assert result == CompanyAdMatchRequest(id=1, matched=False, user_id=1, company_ad_id=1)


def test_create_job_ad_request_wrapper(mocker):
    mocker.patch('src.services.match_request_service.check_if_job_ad_request_already_exists',
                 return_value=None)
    mocker.patch('src.services.job_ad_service.check_job_ad_eligibility', return_value=None)
    mocker.patch('src.services.match_request_service.create_job_ad_request',
                 return_value=JobAdMatchRequest(id=1, matched=False, user_id=1, job_ad_id=1))
    result = create_job_ad_request_wrapper(1, 1)
    assert result == JobAdMatchRequest(id=1, matched=False, user_id=1, job_ad_id=1)


def test_check_if_company_ad_request_already_exists_does_not_exist(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = check_if_company_ad_request_already_exists(1, 1)
    assert result is None


def test_check_if_company_ad_request_already_exists_exists(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[(1, False, 1, None, 1)])
        check_if_company_ad_request_already_exists(1, 1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == 'You have already sent a request to company ad with id 1'


def test_check_if_job_ad_request_already_exists_does_not_exist(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = check_if_job_ad_request_already_exists(1, 1)
    assert result is None


def test_check_if_job_ad_request_already_exists_exists(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[(1, False, 1, 1, None)])
        check_if_job_ad_request_already_exists(1, 1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert ex.value.detail == 'You have already sent a request to job ad with id 1'


def test_match_with_professional_wrapper(mocker):
    mocker.patch('src.services.match_request_service.get_job_match_request_by_id',
                 return_value=JobAdMatchRequest(id=1, matched=False, user_id=1, job_ad_id=1))
    mocker.patch('src.services.match_request_service.check_if_company_is_job_ad_author',
                 return_value=None)
    mocker.patch('src.services.match_request_service.check_if_match_request_status_is_false',
                 return_value=None)
    mocker.patch('src.services.match_request_service.match_with_professional',
                 return_value=HTTPException(status_code=200, detail='You have a match!'))
    result = match_with_professional_wrapper(1, 1)
    assert isinstance(result, HTTPException)
    assert result.status_code == 200
    assert result.detail == 'You have a match!'


def test_match_with_company_wrapper(mocker):
    mocker.patch('src.services.match_request_service.get_company_match_request_by_id',
                 return_value=CompanyAdMatchRequest(id=1, matched=False, user_id=1,
                                                    company_ad_id=1))
    mocker.patch('src.services.match_request_service.check_if_professional_is_company_ad_author',
                 return_value=None)
    mocker.patch('src.services.match_request_service.check_if_match_request_status_is_false',
                 return_value=None)
    mocker.patch('src.services.match_request_service.match_with_company',
                 return_value=HTTPException(status_code=200, detail='You have a match!'))
    result = match_with_company_wrapper(1, 1)
    assert isinstance(result, HTTPException)
    assert result.status_code == 200
    assert result.detail == 'You have a match!'


def test_check_if_match_request_status_is_false_when_true(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1,)])
    result = check_if_match_request_status_is_false(1)
    assert result is None


def test_check_if_match_request_status_is_false_when_false(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[])
        check_if_match_request_status_is_false(1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 403
    assert ex.value.detail == 'Match request with id 1 is already matched'


def test_get_job_match_request_by_id_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, False, 1, 1)])
    result = get_job_match_request_by_id(1)
    assert result == JobAdMatchRequest(id=1, matched=False, user_id=1, job_ad_id=1)


def test_get_job_match_request_by_id_when_not_found(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[])
        get_job_match_request_by_id(1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 404
    assert ex.value.detail == 'Request with id 1 not found'


def test_check_if_company_is_job_ad_author_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1,)])
    result = check_if_company_is_job_ad_author(JobAdMatchRequest(id=1, matched=False, user_id=1,
                                                                 job_ad_id=1), 1)
    assert result is None


def test_check_if_company_is_job_ad_author_when_not_the_author(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[])
        check_if_company_is_job_ad_author(JobAdMatchRequest(id=1, matched=False, user_id=1,
                                                            job_ad_id=1), 1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 403
    assert ex.value.detail == '''Your company is not the recipient of a match request
                    with id 1'''


def test_match_with_professional_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.update_query_transactional', return_value=True)
    result = match_with_professional(JobAdMatchRequest(id=1, matched=False, user_id=1, job_ad_id=1))
    assert result == 'You have a match!'


def test_match_with_professional_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "rollback.return_value": 1
        }
    )
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.data.database.update_query_transactional', return_value=False)
        result = match_with_professional(JobAdMatchRequest(id=1, matched=False, user_id=1,
                                                           job_ad_id=1))
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert isinstance(ex.value.detail, Error)


def test_match_with_company_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.update_query_transactional', return_value=True)
    result = match_with_company(CompanyAdMatchRequest(id=1, matched=False, user_id=1,
                                                      company_ad_id=1), 1)
    assert result == 'You have a match!'


def test_match_with_company_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{
            "commit.return_value": 1
        }
    )
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.data.database.update_query_transactional', return_value=False)
        result = match_with_company(CompanyAdMatchRequest(id=1, matched=False, user_id=1,
                                                          company_ad_id=1), 1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 400
    assert isinstance(ex.value.detail, Error)


def test_get_company_match_request_by_id_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, False, 1, 1)])
    result = get_company_match_request_by_id(1)
    assert result == CompanyAdMatchRequest(id=1, matched=False, user_id=1, company_ad_id=1)


def test_get_company_match_request_by_id_when_not_found(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[])
        get_company_match_request_by_id(1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 404
    assert ex.value.detail == 'Request with id 1 not found'


def test_check_if_professional_is_company_ad_author_when_successful(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1,)])
    result = check_if_professional_is_company_ad_author(CompanyAdMatchRequest(id=1,
                                                                              matched=False,
                                                                              user_id=1,
                                                                              company_ad_id=1), 1)
    assert result is None


def test_check_if_professional_is_company_ad_author_when_not_the_author(mocker):
    with pytest.raises(HTTPException) as ex:
        mocker.patch('src.data.database.read_query', return_value=[])
        check_if_professional_is_company_ad_author(CompanyAdMatchRequest(id=1,
                                                                         matched=False,
                                                                         user_id=1,
                                                                         company_ad_id=1), 1)
    assert isinstance(ex.value, HTTPException)
    assert ex.value.status_code == 403
    assert ex.value.detail == '''Your are not the recipient of a match request
                    with id 1'''


def test_get_match_requests(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, False, 1, 1)])
    result = get_match_requests([1])
    assert result == [CompanyAdMatchRequest(id=1, matched=False, user_id=1, company_ad_id=1)]
