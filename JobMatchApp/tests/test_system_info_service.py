import pytest
from mariadb import Error
from src.data.user_models import UserData
from src.data.app_models import Level, LocationInfo, SkillInfo, SkillStatus, LocationStatus
from src.services.system_info_service import get_locations, get_skills, ad_new_skills, approve_pending_skills, \
    delete_skills_by_ids, get_levels, ad_new_location, approve_pending_locations, delete_locations_by_ids


def get_current_user_admin():
    return UserData(id=3,
                    username='Pesho',
                    hashed_password='hashed_password',
                    user_type_id=3,
                    approved=True,
                    blocked=False,
                    deleted=False)


def get_levels_result():
    return [Level(id=1, level='Beginner'), Level(id=2, level='Intermediate'), Level(id=3, level='Advanced')]


def test_get_locations(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, 'Sofia')])
    result = get_locations('active')
    assert result == [LocationInfo(id=1, city='Sofia')]


def test_get_skills(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, 'Python')])
    result = get_skills('active')
    assert result == [SkillInfo(id=1, name="Python")]


def test_add_new_skill_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=[(1,), (2,)])
    result = ad_new_skills(['java', 'python'], get_current_user_admin())
    assert result == "Skills ['java', 'python'] added"


def test_add_new_skill_when_successful_and_status_pending(mocker, prof):
    mock_conn = mocker.MagicMock()
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=[(1,), (2,)])
    result = ad_new_skills(['java', 'python'], prof)
    assert result == "Skills ['java', 'python'] added"


def test_add_new_skill_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    with pytest.raises(Error) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
        ad_new_skills(['asd'], get_current_user_admin())
    assert isinstance(ex.value, Error)


def test_approve_pending_skills_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=[(1,), (1,)])
    result = approve_pending_skills([1, 2])
    assert result == 'Skills [1, 2] approved'


def test_approve_pending_skills_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    with pytest.raises(Error) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
        approve_pending_skills([1, 2])
    assert isinstance(ex.value, Error)


def test_delete_skills_by_ids_when_successful(mocker):
    mock_conn = mocker.MagicMock()
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=[(1,), (1,)])
    result = delete_skills_by_ids([1, 2])
    assert result == 'Skills [1, 2] deleted'


def test_delete_skills_by_ids_when_unsuccessful(mocker):
    mock_conn = mocker.MagicMock()
    with pytest.raises(Error) as ex:
        mocker.patch('src.data.database.get_connection', return_value=mock_conn)
        mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
        delete_skills_by_ids([1, 2])
    assert isinstance(ex.value, Error)


def test_get_levels(mocker):
    mocker.patch('src.data.database.read_query', return_value=[(1, 'Beginner'), (2, 'Intermediate'), (3, 'Advanced')])
    result = get_levels()
    assert result == get_levels_result()


@pytest.fixture
def admin():
    return UserData(id=3,
                    username='Gosho',
                    hashed_password='hashed_password',
                    user_type_id=3,
                    approved=True,
                    blocked=False,
                    deleted=False)


@pytest.fixture
def prof():
    return UserData(id=1,
                    username='Gosho',
                    hashed_password='hashed_password',
                    user_type_id=1,
                    approved=True,
                    blocked=False,
                    deleted=False)


@pytest.fixture
def location():
    return LocationInfo.from_query(1, 'Sandanski')


def test_ad_new_location_when_not_admin(mocker, prof, location):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value = location)
    expected = f'Location/-s Sandanski added as pending.'
    result = ad_new_location(['Sandanski'], prof)
    assert result == expected


def test_ad_new_locations_when_not_admin(mocker, prof, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value = locations)
    expected = f'Location/-s Sandanski, Vidin added as pending.'
    result = ad_new_location(['Sandanski, Vidin'], prof)
    assert result == expected


def test_ad_new_location_when_admin(mocker, admin, location):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value = location)
    expected = f'Location/-s Sandanski added as active.'
    result = ad_new_location(['Sandanski'], admin)

    assert result == expected


@pytest.fixture
def locations():
    rows = ((1,'Sandanski'), (2, 'Vidin'))
    return [LocationInfo.from_query(*row) for row in rows]


def test_ad_new_locations_when_admin(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value = locations)
    expected = f'Location/-s Sandanski, Vidin added as active.'
    result = ad_new_location(['Sandanski', 'Vidin'], admin)

    assert result == expected


def test_add_new_location_when_admin_raises_error_when_id_not_in_db(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"rollback.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
    with pytest.raises(Error):
        result = ad_new_location(['Sandanski', 'Vidin'], admin)
        assert result == Error


def test_approve_locations_when_admin(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value=locations)
    expected = f'Location/-s 1, 2 approved.'
    result = approve_pending_locations([1, 2])

    assert result == expected


def test_approve_locations_when_admin_raises_error_when_id_not_in_db(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"rollback.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
    with pytest.raises(Error):
        result = approve_pending_locations([1, 2])
        assert result == Error


def test_delete_locations_when_admin(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', return_value = locations)
    expected = f'Location/-s 1, 2 deleted.'
    result = delete_locations_by_ids([1, 2])

    assert result == expected


def test_delete_locations_when_admin_raises_error(mocker, admin, locations):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"rollback.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)

    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)
    with pytest.raises(Error):
        result = delete_locations_by_ids([1, 2])
        assert result == Error