from src.services.twitter_integration_service import get_job_ad_creator


def test_get_job_ad_creator(mocker):
    mocker.patch('src.data.database.read_query', return_value=[('test_user',)])
    result = get_job_ad_creator(1)
    assert result == 'test_user'
