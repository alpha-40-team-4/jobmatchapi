# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=import-error
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments

import pytest
from fastapi import HTTPException, status
from mariadb import Error

from src.data.app_models import Skill, MatchRequest
from src.data.company_ad_models import CompanyAd, CompAdsStatus, CompanyAdCreate, \
    CompanyAdUpdate, CompAdsStatusAll, CompanyAdWithRequests
from src.data.user_models import UserData
from src.services.company_ad_service import get_ads_by_user_id, update, _return_ads_from_data, \
    _return_ads_by_list, create, get_ad_by_id, get_ads_by_user_id_admin, return_ad_by_id, \
    check_company_ad_eligibility, search_comp_ads, get_ads


def generate_company_ad(ad_id: int = 1, ad_status: CompAdsStatusAll = CompAdsStatusAll.ACTIVE,
                        user_id: int = 1):
    return CompanyAd(id=ad_id,
                     salary_min=100,
                     salary_max=200,
                     description='descr',
                     location_id=1,
                     remote=True,
                     status=ad_status,
                     user_id=user_id,
                     skills_levels=[Skill(skill_id=1, level_id=1)])


def generate_company_ad_update(salary_min: int | None = None, salary_max: int | None = None,
                               description: str | None = None, remote: bool | None = None,
                               ad_status: CompAdsStatus | None = None,
                               location_id: int | None = None,
                               skills_levels: list[Skill] | None = None):
    return CompanyAdUpdate(salary_min=salary_min,
                           salary_max=salary_max,
                           description=description,
                           remote=remote,
                           status=ad_status,
                           location_id=location_id,
                           skills_levels=skills_levels)


def generate_user_data(user_id: int = 1, user_type_id: int = 1, deleted: bool = False):
    return UserData(id=user_id,
                    username='user',
                    hashed_password='',
                    user_type_id=user_type_id,
                    approved=True,
                    blocked=False,
                    deleted=deleted)


def test_create(mocker):
    company_ad_create = CompanyAdCreate(
        salary_min=100, salary_max=200, description='descr',
        location_id=1, remote=True,
        status=CompAdsStatus.ACTIVE,
        skills_levels=[Skill(skill_id=1, level_id=1)]
    )
    ad_id = 1
    user_id = 1

    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(**{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', return_value=ad_id)
    expected_res = company_ad_create.dict()
    expected_res.update({'id': ad_id, 'user_id': user_id})
    result = create(c_ad=company_ad_create, user_id=user_id)
    assert result == expected_res


ads_update = [
    generate_company_ad_update(salary_min=150),
    generate_company_ad_update(salary_max=150),
    generate_company_ad_update(description='new description'),
    generate_company_ad_update(remote=False),
    generate_company_ad_update(ad_status=CompAdsStatus.PRIVATE),
    generate_company_ad_update(location_id=2),
    generate_company_ad_update(skills_levels=[Skill(skill_id=1, level_id=2)]),
    generate_company_ad_update(skills_levels=[Skill(skill_id=2, level_id=2)]),
    generate_company_ad_update(skills_levels=[Skill(skill_id=1, level_id=2),
                                              Skill(skill_id=2, level_id=2)]),
    generate_company_ad_update(salary_min=150, salary_max=150),
    generate_company_ad_update(remote=False, skills_levels=[Skill(skill_id=1, level_id=2)])
]


@pytest.mark.parametrize('ad_update', ads_update)
def test_update(mocker, ad_update):
    ad = generate_company_ad()
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(**{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.update_query_transactional', return_value=1)
    mocker.patch('src.data.database.update_query', return_value=1)
    mocker.patch('src.data.database.process_query_transactional', return_value=1)

    if ad_update.skills_levels:
        skills_ids = set(
            [s.skill_id for s in ad.skills_levels]
            + [s.skill_id for s in ad_update.skills_levels]
        )
        new = []
        for skill_id in skills_ids:
            current = next((s for s in ad.skills_levels if s.skill_id == skill_id), None)
            updated = next((s for s in ad_update.skills_levels if s.skill_id == skill_id), None)
            new.append(updated if updated else current)
    else:
        new = ad.skills_levels

    expected_res = ad.copy(update=ad_update.dict(exclude_none=True))
    expected_res.skills_levels = new
    result = update(ad.id, ad_update, ad.user_id)

    assert result == expected_res


ad_not_update = [
    generate_company_ad(user_id=2),
    generate_company_ad(ad_status=CompAdsStatusAll.MATCHED),
    generate_company_ad(ad_status=CompAdsStatusAll.ACTIVE)
]


@pytest.mark.parametrize('ad', ad_not_update[:2])
def test_update_raise_exception(mocker, ad):
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    with pytest.raises(HTTPException) as ex:
        result = update(ad.id, ads_update[0], 1)
        assert result is None
        assert ex.type == HTTPException


@pytest.mark.parametrize('ad, updated', [(ad_not_update[2], ads_update[4]),
                                         (generate_company_ad(), ads_update[6]),
                                         (generate_company_ad(), ads_update[8])])
def test_update_raise_error(mocker, ad, updated):
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(**{"commit.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.update_query_transactional', side_effect=Error)
    mocker.patch('src.data.database.process_query_transactional', side_effect=0)
    with pytest.raises(Error) as ex:
        result = update(ad.id, updated, 1)
        assert result is None
        assert ex.type == Error


user = generate_user_data()
admin = generate_user_data(user_id=2, user_type_id=3)


@pytest.mark.parametrize('user_data', [user, admin])
def test_get_ad_by_id_author_or_admin(mocker, user_data):
    ad = generate_company_ad()
    match = MatchRequest(id=1, matched=True, user_id=5)
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    mocker.patch('src.data.database.read_query',
                 return_value=[(match.id, match.matched, match.user_id)])
    expected_res = CompanyAdWithRequests.from_ad(ad, [match])
    result = get_ad_by_id(ad.id, user_data)
    assert result == expected_res


hidden_ad = generate_company_ad(ad_status=CompAdsStatusAll.HIDDEN)
matched_ad = generate_company_ad(ad_status=CompAdsStatusAll.MATCHED)


@pytest.mark.parametrize('ad', [hidden_ad, matched_ad])
def test_get_ad_by_id_raises_exception(mocker, ad):
    current_user = generate_user_data(user_id=2)
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    with pytest.raises(HTTPException) as ex:
        result = get_ad_by_id(ad.id, current_user)
        assert result is None
        assert ex.type == HTTPException


def test_get_ad_by_id(mocker):
    ad = generate_company_ad()
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=ad)
    result = get_ad_by_id(ad.id, generate_user_data(user_id=2))
    assert result == ad


def generate_data(num_elements):
    comp_ad = []
    for i in range(num_elements):
        comp_ad.append((1, 100, 500, 'descr', True, 'active', None, 1, 1, 1))
    return comp_ad


@pytest.mark.parametrize('num_elements', [1, 5, 10])
def test_get_ads_by_user_id(mocker, num_elements):
    mocker.patch('src.data.database.read_query', return_value=generate_data(num_elements))
    result = get_ads_by_user_id(1)
    assert isinstance(result, list)
    assert result is not None


deleted_user = generate_user_data(deleted=True)
user_company = generate_user_data(user_type_id=2)


@pytest.mark.parametrize('current_user', [None, deleted_user, user_company])
def test_get_ads_by_user_id_admin_raises_exception(mocker, current_user):
    mocker.patch('src.services.users_service.get_user_by_id', return_value=current_user)
    with pytest.raises(HTTPException) as ex:
        result = get_ads_by_user_id_admin(1)
        assert result is None
        assert ex.type == HTTPException


def test_get_ads_by_user_id_admin(mocker):
    ad = generate_company_ad()
    mocker.patch('src.services.users_service.get_user_by_id', return_value=generate_user_data())
    mocker.patch('src.services.company_ad_service.get_ads_by_user_id', return_value=[ad])
    result = get_ads_by_user_id_admin(1)
    assert isinstance(result, list)
    assert len(result) == 1
    assert result[0] == ad


def test_search_comp_ads_raises_exception():
    with pytest.raises(HTTPException) as ex:
        search_comp_ads(100, 1, 0, None, None, [], 0)
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST


params = [
    {'min_salary': 100, 'max_salary': None, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': [], 'skills_threshold': 0},
    {'min_salary': None, 'max_salary': 200, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': [], 'skills_threshold': 0},
    {'min_salary': 100, 'max_salary': 200, 'salary_threshold': 0.10,
     'location_id': None, 'remote': None, 'skills': [], 'skills_threshold': 0},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': None, 'skills': [], 'skills_threshold': 0},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': None, 'remote': True, 'skills': [], 'skills_threshold': 0},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': False, 'skills': [], 'skills_threshold': 0},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': None, 'remote': None, 'skills': ['1,1'],
     'skills_threshold': 0},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': None, 'remote': True, 'skills': ['1,1', '2,2'],
     'skills_threshold': 1},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': None, 'remote': True, 'skills': ['1,1', '2,2'],
     'skills_threshold': 3},
    {'min_salary': None, 'max_salary': None, 'salary_threshold': 0,
     'location_id': 1, 'remote': True, 'skills': ['1,1', '2,2'],
     'skills_threshold': 1}
]
company_ad1 = generate_company_ad()
company_ad2 = generate_company_ad(ad_id=2)
expected = [company_ad1, company_ad2]

list_ids = [(1,), (2,)]


@pytest.mark.parametrize('param', params)
def test_get_ads(mocker, param):
    mocker.patch('src.data.database.read_query', return_value=list_ids)
    mocker.patch('src.services.company_ad_service._return_ads_by_list', return_value=expected)
    result = get_ads(**param)
    assert result == expected


row1 = (1, 100, 500, 'descr', True, 'active', None, 1, 1, 1)
row2 = (1, 100, 500, 'descr', True, 'active', None, 1, 2, 1)
row3 = (2, 100, 500, 'descr', True, 'active', None, 1, 1, 1)


def test_return_ads_from_data():
    result = _return_ads_from_data([row1, row2, row3])
    assert isinstance(result, list)
    assert len(result) == 2
    assert isinstance(result[0].skills_levels, list)
    assert len(result[0].skills_levels) == 2
    assert len(result[1].skills_levels) == 1


@pytest.mark.parametrize('c_id, data', [([], []),
                                        ([(1,)], [row1, row2]),
                                        ([(1,), (2,)], [row1, row2, row3])])
def test_return_ads_by_list(mocker, c_id, data):
    mocker.patch('src.data.database.read_query', return_value=data)
    result = _return_ads_by_list(c_id)
    assert isinstance(result, list)
    assert len(result) == len(c_id)


def test_return_ad_by_id_raises_exception(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    with pytest.raises(HTTPException) as ex:
        result = return_ad_by_id(1)
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.parametrize('data', [[row1], [row1, row2]])
def test_return_ad_by_id(mocker, data):
    mocker.patch('src.data.database.read_query', return_value=data)
    result = return_ad_by_id(1)
    assert result is not None
    assert result.id == row1[0]
    assert result.salary_min == row1[1]
    assert result.salary_max == row1[2]
    assert result.description == row1[3]
    assert result.remote == row1[4]
    assert result.status == row1[5]
    assert len(result.skills_levels) == len(data)


def test_check_company_ad_eligibility_raises_exception(mocker):
    mocker.patch('src.data.database.read_query', return_value=1)
    with pytest.raises(HTTPException) as ex:
        result = check_company_ad_eligibility(1)
        assert result is None
        assert ex.type == status.HTTP_403_FORBIDDEN


def test_check_company_ad_eligibility(mocker):
    mocker.patch('src.data.database.read_query', return_value=[])
    result = check_company_ad_eligibility(1)
    assert result is None


@pytest.fixture
def almost_valid_job_ad(ad_status: CompAdsStatusAll = CompAdsStatusAll.ACTIVE):
    return CompanyAdCreate(
        salary_min=100,
        salary_max=200,
        description='descr',
        location_id=1,
        remote=True,
        status=ad_status,
        skills_levels=[])


@pytest.fixture
def almost_valid_job_ad_update(ad_status: CompAdsStatusAll = CompAdsStatusAll.ACTIVE):
    return CompanyAdUpdate(
        salary_min=100,
        salary_max=200,
        description='descr',
        location_id=1,
        remote=True,
        status=ad_status,
        skills_levels=[])


@pytest.fixture
def c_ad(ad_id=1, ad_status: CompAdsStatusAll = CompAdsStatusAll.ACTIVE,
         user_id=1):
    return CompanyAd(id=ad_id,
                     salary_min=100,
                     salary_max=200,
                     description='descr',
                     location_id=1,
                     remote=True,
                     status=ad_status,
                     user_id=user_id,
                     skills_levels=[Skill(skill_id=1, level_id=1)])


def test_create_raises_error_when_data_not_valid(mocker, almost_valid_job_ad):
    mock_conn = mocker.MagicMock()
    mock_conn.configure_mock(
        **{"rollback.return_value": 1})
    mocker.patch('src.data.database.get_connection', return_value=mock_conn)
    mocker.patch('src.data.database.process_query_transactional', side_effect=Error)

    with pytest.raises(Error) as ex:
        job_ad = create(almost_valid_job_ad, 1)
        assert job_ad is None
        assert ex.type == Error
        mock_conn.rollback().assert_called_once()


def test_get_ads_by_user_id_when_returns_empty_list(mocker):
    mocker.patch('src.data.database.read_query', return_value=None)
    result = get_ads_by_user_id(1)
    assert result == []


def test_search_comp_ads(mocker, c_ad):
    mocker.patch('src.services.company_ad_service.get_ads', return_value=[c_ad])
    result = search_comp_ads(100, 1000, 0, None, None, ['1,1'], 2)
    assert result == [c_ad]
