import pytest
from fastapi import HTTPException, status

from src.data.app_models import CompanyAdMatchRequest
from src.data.company_ad_models import CompanyAd, CompAdsStatusAll
from src.data.user_models import UserData
from src.services.info_service import get_professional_info, get_company_info, search_companies,\
    search_professionals, update_company, update_prof, get_user_info_by_id, \
    get_user_info_by_userdata
from src.data.info_models import CompanyInfo, CompanyInfoUpdate, \
    CompanyInfoFull, ProfInfo, ProfInfoUpdate,\
    ProfInfoFull


def generate_company_info():
    return CompanyInfo(id=1,
                       name='test_company',
                       description=None,
                       email='email@test.te',
                       contacts=None,
                       logo=None,
                       location_id=None)


def generate_company_info_update(name: str | None = None, description: str | None = None,
                                 contacts: str | None = None, logo: str | None = None,
                                 location_id: int | None = None):
    return CompanyInfoUpdate(name=name,
                             description=description,
                             contacts=contacts,
                             logo=logo,
                             location_id=location_id)


def generate_company_ad(user_id: int = 1, ad_status: CompAdsStatusAll = CompAdsStatusAll.ACTIVE):
    return CompanyAd(id=1,
                     user_id=user_id,
                     status=ad_status,
                     salary_min=100,
                     salary_max=200,
                     description='test',
                     location_id=None,
                     remote=True,
                     skills_levels=[])


def generate_professional_info(visible_matches: bool | None = None):
    return ProfInfo(id=1,
                    summary=None,
                    active=None,
                    photo=None,
                    location_id=None,
                    email='test@test.te',
                    first_name='test_name',
                    last_name='test_name',
                    main_company_ad_id=None,
                    visible_matches=visible_matches)


def generate_professional_info_update(summary: str | None = None, active: bool | None = None,
                                      photo: str | None = None, location_id: int | None = None,
                                      first_name: str | None = None, last_name: str | None = None,
                                      main_company_ad_id: int | None = None,
                                      visible_matches: bool | None = None):
    return ProfInfoUpdate(summary=summary,
                          active=active,
                          photo=photo,
                          location_id=location_id,
                          first_name=first_name,
                          last_name=last_name,
                          main_company_ad_id=main_company_ad_id,
                          visible_matches=visible_matches)


def generate_user_data(user_type_id: int = 1, deleted: bool = False):
    return UserData(id=1,
                    username='test',
                    hashed_password='test',
                    user_type_id=user_type_id,
                    approved=True,
                    blocked=False,
                    deleted=deleted)


comp_info_update = [
    generate_company_info_update(name='new name'),
    generate_company_info_update(description='updated'),
    generate_company_info_update(contacts='contacts'),
    generate_company_info_update(logo='new logo'),
    generate_company_info_update(location_id=1),
    generate_company_info_update(name='new name', description='updated', logo='new logo'),
    generate_company_info_update(name='new name', description='updated', contacts='contacts',
                                 logo='new logo', location_id=1)
]


@pytest.mark.parametrize('info_update', comp_info_update)
def test_update_company(mocker, info_update):
    mocker.patch('src.data.database.update_query', return_value=1)
    mocker.patch('src.data.database.upload_image', return_value='new logo')
    company = generate_company_info()
    expected = company.copy(update=info_update.dict(exclude_none=True))
    result = update_company(company, info_update)
    assert result == expected


company_info = generate_company_info()

comp_info_update_no_change = [
    generate_company_info_update(name=company_info.name),
    generate_company_info_update(description=company_info.description),
    generate_company_info_update(contacts=company_info.contacts),
    generate_company_info_update(logo=company_info.logo),
    generate_company_info_update(location_id=company_info.location_id),
    generate_company_info_update(name=company_info.name, description=company_info.description,
                                 logo=company_info.logo),
    generate_company_info_update(name=company_info.name, description=company_info.description,
                                 contacts=company_info.contacts, logo=company_info.logo,
                                 location_id=company_info.location_id)
]


@pytest.mark.parametrize('info_update', comp_info_update_no_change)
def test_update_company_returns_str(mocker, info_update):
    mocker.patch('src.data.database.update_query', return_value=0)
    result = update_company(generate_company_info(), info_update)
    assert isinstance(result, str)
    assert result == 'No changes made to company info'


comp_ads = [
    generate_company_ad(user_id=2),
    generate_company_ad(ad_status=CompAdsStatusAll.MATCHED),
    generate_company_ad(ad_status=CompAdsStatusAll.HIDDEN),
    generate_company_ad(ad_status=CompAdsStatusAll.DELETED),
    generate_company_ad(ad_status=CompAdsStatusAll.PRIVATE)
]


@pytest.mark.parametrize('company_ad', comp_ads)
def test_update_prof_raises_exception(mocker, company_ad):
    mocker.patch('src.services.company_ad_service.return_ad_by_id', return_value=company_ad)
    info_update = generate_professional_info_update(main_company_ad_id=company_ad.id)
    with pytest.raises(HTTPException) as ex:
        result = update_prof(generate_professional_info(), info_update)
        assert result == ex.type
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST


professional_info = generate_professional_info()

prof_info_update_no_change = [
    generate_professional_info_update(summary=professional_info.summary),
    generate_professional_info_update(active=professional_info.active),
    generate_professional_info_update(photo=professional_info.photo),
    generate_professional_info_update(location_id=professional_info.location_id),
    generate_professional_info_update(first_name=professional_info.first_name),
    generate_professional_info_update(last_name=professional_info.last_name),
    generate_professional_info_update(main_company_ad_id=professional_info.main_company_ad_id),
    generate_professional_info_update(visible_matches=professional_info.visible_matches),
    generate_professional_info_update(summary=professional_info.summary,
                                      photo=professional_info.photo),
    generate_professional_info_update(first_name=professional_info.first_name,
                                      last_name=professional_info.last_name,
                                      visible_matches=professional_info.visible_matches)
]


@pytest.mark.parametrize('info_update', prof_info_update_no_change)
def test_update_prof_returns_str(mocker, info_update):
    mocker.patch('src.services.company_ad_service.return_ad_by_id',
                 return_value=generate_company_ad())
    mocker.patch('src.data.database.update_query', return_value=0)
    result = update_prof(professional_info, info_update)
    assert isinstance(result, str)
    assert result == 'No changes made to professional info'


pr_info_update = [
    generate_professional_info_update(summary='new summery'),
    generate_professional_info_update(active=True),
    generate_professional_info_update(photo='new photo'),
    generate_professional_info_update(location_id=1),
    generate_professional_info_update(first_name='new name'),
    generate_professional_info_update(last_name='new name'),
    generate_professional_info_update(main_company_ad_id=1),
    generate_professional_info_update(visible_matches=False),
    generate_professional_info_update(summary='new summary', photo='new photo'),
    generate_professional_info_update(first_name='new name',
                                      last_name='new name', visible_matches=False)
]


@pytest.mark.parametrize('info_update', pr_info_update)
def test_update_prof(mocker, info_update):
    mocker.patch('src.services.company_ad_service.return_ad_by_id',
                 return_value=generate_company_ad())
    mocker.patch('src.data.database.upload_image', return_value='new photo')
    mocker.patch('src.data.database.update_query', return_value=1)
    professional = generate_professional_info()
    expected = professional.copy(update=info_update.dict(exclude_none=True))
    result = update_prof(professional, info_update)
    assert result == expected


@pytest.mark.parametrize('invalid_user', [None, generate_user_data(deleted=True)])
def test_get_user_info_by_id_raises_exception(mocker, invalid_user):
    mocker.patch('src.services.users_service.get_user_by_id', return_value=invalid_user)
    with pytest.raises(HTTPException) as ex:
        result = get_user_info_by_id(1)
        assert result == ex.type
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST


def test_get_user_info_by_id(mocker):
    test_user_data = generate_user_data()
    user = {}
    mocker.patch('src.services.users_service.get_user_by_id', return_value=test_user_data)
    mocker.patch('src.services.info_service.get_user_info_by_userdata', return_value=user)
    expected = {'username': test_user_data.username,
                'approved': test_user_data.approved,
                'blocked': test_user_data.blocked,
                'user info': user}
    result = get_user_info_by_id(test_user_data.id)
    assert result == expected


userdata_not_found = [
    (generate_user_data(), None),
    (generate_user_data(user_type_id=2), None)
]


@pytest.mark.parametrize('user, user_profile', userdata_not_found)
def test_get_user_info_by_userdata_raises_exception(mocker, user, user_profile):
    mocker.patch('src.services.info_service.get_professional_info', return_value=user_profile)
    mocker.patch('src.services.info_service.get_company_info', return_value=user_profile)
    with pytest.raises(HTTPException) as ex:
        result = get_user_info_by_userdata(user)
        assert result is None
        assert ex.type == HTTPException
        assert ex.value.status_code == status.HTTP_400_BAD_REQUEST
        assert ex.value.detail == 'User not found'


data_user = generate_user_data()

userdata_pass = [
    (data_user, data_user.dict()),
    (generate_user_data(user_type_id=2), data_user.dict()),
    (generate_user_data(user_type_id=3), {'username': data_user.username, 'user_type_id': 3})
]


@pytest.mark.parametrize('user_d, user_profile', userdata_pass)
def test_get_user_info_by_userdata(mocker, user_d, user_profile):
    mocker.patch('src.services.info_service.get_professional_info', return_value=user_profile)
    mocker.patch('src.services.info_service.get_company_info', return_value=user_profile)
    result = get_user_info_by_userdata(user_d)
    assert result == user_profile


prof_info_data = [
    (None, None),
    (generate_professional_info(visible_matches=True), {
        'user info': generate_professional_info(visible_matches=True),
                                                        'number of active ads': 1,
                                                        'matched ads': []}),
    (generate_professional_info(), {'user info': generate_professional_info(),
                                    'number of active ads': 1})
]


@pytest.mark.parametrize('user_info, expected', prof_info_data)
def test_get_professional_info(mocker, user_info, expected):
    mocker.patch('src.services.users_service.get_professional_by_id', return_value=user_info)
    mocker.patch('src.services.company_ad_service.get_ads_by_user_id',
                 return_value=[generate_company_ad()])
    mocker.patch('src.services.match_request_service.get_match_requests', return_value=[])
    result = get_professional_info(1)
    assert result == expected


comp_info_data = [
    (None, None),
    (generate_company_info(), {'user info': generate_company_info(),
                               'number of active ads': 0,
                               'number of matched ads': 0})
]


@pytest.mark.parametrize('user_info, expected', comp_info_data)
def test_get_company_info(mocker, user_info, expected):
    mocker.patch('src.services.users_service.get_company_by_id', return_value=user_info)
    mocker.patch('src.services.job_ad_service.get_ads_by_user_id', return_value=[])
    result = get_company_info(1)
    assert result == expected


params = [{'name': 'Comp', 'description': None, 'location_id': None},
          {'name': None, 'description': 'asi', 'location_id': None},
          {'name': None, 'description': None, 'location_id': 1},
          {'name': 'Comp', 'description': 'asi', 'location_id': 1}]

company1_info_full = CompanyInfoFull(id=1,
                                     name='Comp1',
                                     description='basic desc',
                                     email='company1@mail.com',
                                     contacts=None,
                                     logo=None,
                                     location_id=1,
                                     number_active_ads=5,
                                     number_matches=2)
company2_info_full = company1_info_full.copy(
    update={'id': 2, 'name': 'Comp2', 'email': 'company2@mail.com',
                                                     'number_active_ads': 5})

expected_info = [company1_info_full, company2_info_full]
active_ads_output = [(1, 5), (2, 5)]
matched_ads_output = [(1, 2), (2, 2)]


@pytest.mark.parametrize('param', params)
def test_search_companies(mocker, param):
    mocker.patch('src.data.database.read_query',
                 side_effect=[[(1, 'Comp1', 'basic desc', 'company1@mail.com', None,
                                None, 1),
                               (2, 'Comp2', 'basic desc',
                                'company2@mail.com', None,
                                None, 1)],
                              active_ads_output, matched_ads_output])
    result = search_companies(**param)
    assert result is not None
    assert isinstance(result, list)
    assert result[0] == company1_info_full
    assert result[1] == company2_info_full


params1 = [
    {'first_name': 'esh', 'last_name': None, 'summary': None, 'location_id': None},
    {'first_name': None, 'last_name': 'esh', 'summary': None, 'location_id': None},
    {'first_name': None, 'last_name': None, 'summary': 'esh', 'location_id': None},
    {'first_name': None, 'last_name': None, 'summary': None, 'location_id': 1}
]


def create_company_ad_match_request():
    return CompanyAdMatchRequest(id=2,
                                 matched=True,
                                 user_id=3,
                                 company_ad_id=4
                                 )


professional1_full_info = ProfInfoFull(id=1,
                                       summary='I am Pesho',
                                       active=True,
                                       photo=None,
                                       location_id=1,
                                       email='peshopeshev@gmail.com',
                                       first_name='Pesho',
                                       last_name='Peshev',
                                       main_company_ad_id=None,
                                       visible_matches=True,
                                       number_of_active_ads=None,
                                       matches=[create_company_ad_match_request()])

professional2_full_info = professional1_full_info.copy(
    update={'id': 2, 'summary': 'I am Kiro',
            'email': 'krio@gmail.com', 'first_name': 'Kiro',
            'last_name': 'Kirov', 'matches': None})

expected_prof = [professional1_full_info]
active_ads_num = [(1, 2), (2, 2)]
matched_ads_dict_output = [(1, 2, True, 3, 4)]


@pytest.mark.parametrize('param', params1)
def test_search_professionals(mocker, param):
    mocker.patch('src.data.database.read_query',
                 side_effect=[[(1, 'I am Pesho', True, None, 1,
                                'peshopeshev@gmail.com', 'Pesho', 'Peshev', None,
                                True)], active_ads_num, matched_ads_dict_output])
    result = search_professionals(**param)
    assert result is not None
    assert isinstance(result, list)
    assert isinstance(result[0], ProfInfoFull)
    assert result[0].first_name == 'Pesho'
    assert result[0].matches == [create_company_ad_match_request()]
    assert len(result) == 1
