-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema team_4_job_match
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema team_4_job_match
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `team_4_job_match` DEFAULT CHARACTER SET utf8mb4 ;
USE `team_4_job_match` ;

-- -----------------------------------------------------
-- Table `team_4_job_match`.`user_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`user_types` (
  `id` INT(11) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(65) NOT NULL,
  `user_types_id` INT(11) NOT NULL,
  `approved` TINYINT(4) NOT NULL DEFAULT 0,
  `blocked` TINYINT(4) NOT NULL DEFAULT 0,
  `deleted` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_users_user_types1_idx` (`user_types_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_user_types1`
    FOREIGN KEY (`user_types_id`)
    REFERENCES `team_4_job_match`.`user_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 51
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`locations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`locations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  `status` ENUM('active', 'pending', 'deleted') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 32
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`companies` (
  `company_name` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `description` VARCHAR(1000) CHARACTER SET 'utf8mb3' NULL DEFAULT NULL,
  `contacts` VARCHAR(200) CHARACTER SET 'utf8mb3' NULL DEFAULT NULL,
  `picture_logo` BLOB NULL DEFAULT NULL,
  `location_id` INT(11) NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_company_info_location1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_companies_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_companies_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `team_4_job_match`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_info_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `team_4_job_match`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`professionals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`professionals` (
  `summary` VARCHAR(1000) CHARACTER SET 'utf8mb3' NULL DEFAULT NULL,
  `active` TINYINT(4) NOT NULL DEFAULT 1,
  `photo` BLOB NULL DEFAULT NULL,
  `location_id` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  `last_name` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  `main_company_ad_id` INT(11) NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  `visible_matches` TINYINT(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`users_id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_professional_info_location1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_professional_info_company_ad1_idx` (`main_company_ad_id` ASC) VISIBLE,
  INDEX `fk_professionals_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_professional_info_company_ad1`
    FOREIGN KEY (`main_company_ad_id`)
    REFERENCES `team_4_job_match`.`company_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professional_info_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `team_4_job_match`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professionals_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `team_4_job_match`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`company_ads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`company_ads` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `salary_min` INT(11) NOT NULL,
  `salary_max` INT(11) NOT NULL,
  `description` VARCHAR(1000) CHARACTER SET 'utf8mb3' NOT NULL,
  `remote` TINYINT(4) NOT NULL DEFAULT 1,
  `status` ENUM('active', 'private', 'hidden', 'matched', 'deleted') NOT NULL,
  `location_id` INT(11) NULL DEFAULT NULL,
  `professionals_users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_company_ads_location1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_company_ads_professionals1_idx` (`professionals_users_id` ASC) VISIBLE,
  CONSTRAINT `fk_company_ads_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `team_4_job_match`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_ads_professionals1`
    FOREIGN KEY (`professionals_users_id`)
    REFERENCES `team_4_job_match`.`professionals` (`users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 50
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`levels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`levels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `level` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`skills` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `status` ENUM('active', 'pending', 'deleted') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`company_ads_has_skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`company_ads_has_skills` (
  `company_ads_id` INT(11) NOT NULL,
  `skills_id` INT(11) NOT NULL,
  `levels_id` INT(11) NOT NULL,
  PRIMARY KEY (`company_ads_id`, `skills_id`),
  INDEX `fk_company_ads_has_skills_skills1_idx` (`skills_id` ASC) VISIBLE,
  INDEX `fk_company_ads_has_skills_company_ads1_idx` (`company_ads_id` ASC) VISIBLE,
  INDEX `fk_company_ads_has_skills_levels1_idx` (`levels_id` ASC) VISIBLE,
  CONSTRAINT `fk_company_ads_has_skills_company_ads1`
    FOREIGN KEY (`company_ads_id`)
    REFERENCES `team_4_job_match`.`company_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_ads_has_skills_levels1`
    FOREIGN KEY (`levels_id`)
    REFERENCES `team_4_job_match`.`levels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_ads_has_skills_skills1`
    FOREIGN KEY (`skills_id`)
    REFERENCES `team_4_job_match`.`skills` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`job_ads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`job_ads` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `salary_min` INT(11) NOT NULL,
  `salary_max` INT(11) NOT NULL,
  `description` VARCHAR(1000) CHARACTER SET 'utf8mb3' NOT NULL,
  `location_id` INT(11) NULL DEFAULT NULL,
  `remote` TINYINT(4) NOT NULL DEFAULT 0,
  `status` ENUM('active', 'matched', 'deleted') NOT NULL DEFAULT 'active',
  `companies_users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_job_ad_location1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_job_ads_companies1_idx` (`companies_users_id` ASC) VISIBLE,
  CONSTRAINT `fk_job_ad_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `team_4_job_match`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_companies1`
    FOREIGN KEY (`companies_users_id`)
    REFERENCES `team_4_job_match`.`companies` (`users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 208
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`job_ads_has_skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`job_ads_has_skills` (
  `job_ads_id` INT(11) NOT NULL,
  `skills_id` INT(11) NOT NULL,
  `levels_id` INT(11) NOT NULL,
  PRIMARY KEY (`job_ads_id`, `skills_id`),
  INDEX `fk_job_ads_has_skills_skills1_idx` (`skills_id` ASC) VISIBLE,
  INDEX `fk_job_ads_has_skills_job_ads1_idx` (`job_ads_id` ASC) VISIBLE,
  INDEX `fk_job_ads_has_skills_levels1_idx` (`levels_id` ASC) VISIBLE,
  CONSTRAINT `fk_job_ads_has_skills_job_ads1`
    FOREIGN KEY (`job_ads_id`)
    REFERENCES `team_4_job_match`.`job_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_has_skills_levels1`
    FOREIGN KEY (`levels_id`)
    REFERENCES `team_4_job_match`.`levels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ads_has_skills_skills1`
    FOREIGN KEY (`skills_id`)
    REFERENCES `team_4_job_match`.`skills` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `team_4_job_match`.`match_requests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`match_requests` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `matched` TINYINT(4) NOT NULL DEFAULT 0,
  `users_id` INT(11) NOT NULL,
  `job_ad_id` INT(11) NULL DEFAULT NULL,
  `company_ad_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_match_request_auth1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_match_request_job_ad1_idx` (`job_ad_id` ASC) VISIBLE,
  INDEX `fk_match_request_company_ad1_idx` (`company_ad_id` ASC) VISIBLE,
  CONSTRAINT `fk_match_request_auth1`
    FOREIGN KEY (`users_id`)
    REFERENCES `team_4_job_match`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_request_company_ad1`
    FOREIGN KEY (`company_ad_id`)
    REFERENCES `team_4_job_match`.`company_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_request_job_ad1`
    FOREIGN KEY (`job_ad_id`)
    REFERENCES `team_4_job_match`.`job_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 47
DEFAULT CHARACTER SET = latin1;

USE `team_4_job_match` ;

-- -----------------------------------------------------
-- Placeholder table for view `team_4_job_match`.`job_ads_skills_flat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `team_4_job_match`.`job_ads_skills_flat` (`id` INT, `salary_min` INT, `salary_max` INT, `description` INT, `location_id` INT, `remote` INT, `status` INT, `companies_users_id` INT, `skills_flat` INT);

-- -----------------------------------------------------
-- View `team_4_job_match`.`job_ads_skills_flat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `team_4_job_match`.`job_ads_skills_flat`;
USE `team_4_job_match`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Team4`@`%` SQL SECURITY DEFINER VIEW `team_4_job_match`.`job_ads_skills_flat` AS select `j`.`id` AS `id`,`j`.`salary_min` AS `salary_min`,`j`.`salary_max` AS `salary_max`,`j`.`description` AS `description`,`j`.`location_id` AS `location_id`,`j`.`remote` AS `remote`,`j`.`status` AS `status`,`j`.`companies_users_id` AS `companies_users_id`,group_concat(concat_ws(':',`js`.`skills_id`,`js`.`levels_id`) separator ',') AS `skills_flat` from ((`team_4_job_match`.`job_ads` `j` join `team_4_job_match`.`job_ads_has_skills` `js`) join `team_4_job_match`.`skills` `s`) where `js`.`job_ads_id` = `j`.`id` and `s`.`id` = `js`.`skills_id` and `s`.`status` = 'active' group by `j`.`id`,  `j`.`salary_min`,  `j`.`salary_max`, `j`.`description`, `j`.`location_id`,  `j`.`remote`, `j`.`status`, `j`.`companies_users_id`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
