use team_4_job_match;

INSERT into levels(id, level) VALUES(1, 'beginner'), (2, 'advanced'), (3, 'expert');

INSERT into skills(id, name, status) VALUES(1, 'python', 'active'), (2, 'java', 'active'), (3, 'php', 'pending'), (4, 'javascript', 'deleted');

INSERT into locations(id, city, status) VALUES(1, 'Sofia', 'active'), (2, 'Plovdiv', 'active'), (3, 'Burgas', 'active'), (4, 'Varna', 'active');

INSERT into user_types(id, type) VALUES(1, 'professionals'), (2, 'companies'), (3, 'admin');

INSERT into users(id, username, password, user_types_id, approved, blocked, deleted) VALUES(1, 'Professional1', '1234', 1, True, False, False);

INSERT into professionals(summary, active, photo, location_id, email, first_name, last_name, main_company_ad_id, users_id, visible_matches)
VALUES (null, True, null, 1, 'prof1@gmail.com', 'Kiro', 'Kirov', null, 1, True);

INSERT into users(id, username, password, user_types_id, approved, blocked, deleted) VALUES(2, 'TestCompanyUser', '1234', 2, True, False, False);

INSERT into companies(company_name, email, description, contacts, picture_logo, location_id, users_id)
VALUES ('TestCompanyInc', 'testcompinc1@gmail.com', 'some description', null, null, 1, 2);

INSERT into users(id, username, password, user_types_id, approved, blocked, deleted) VALUES(3, 'admin', '1234', 3, True, False, False);

INSERT into company_ads(id, salary_min, salary_max, description, remote, status, location_id, professionals_users_id)
VALUES (1, 1600, 2200, 'my first company ad', True, 'active', 1, 1);

INSERT into job_ads(id, salary_min, salary_max, description, location_id, remote, status, companies_users_id)
VALUES (1, 1700, 2300, 'our first job ad', null, True, 'active', 2);

INSERT into company_ads_has_skills(company_ads_id, skills_id, levels_id) VALUES(1, 1, 3);
INSERT into job_ads_has_skills(job_ads_id, skills_id, levels_id) VALUES(1, 2, 2);
